## LICENCE

Cecill licensed code

Company: REEDS

Author: ROPARS Benoît (benoit.ropars@solutionsreeds.fr)

All codes are made available free of charge. These are codes from the manufacturer's documentation.
Libraries using this code are available under a proprietary license.We can also develop specific solutions for your needs. You can contact us via the following address: contact@solutionsreeds.fr

## MODIFICATION
- v2.10.4 (11/08/2023): Correctif bug de reconnexion
- v2.10.3 (05/07/2023): correctif bug broken pipe
- v2.10.2 (05/07/2023): correctif bug du write avec le string en params (variable inacessible)
- v2.10.1 (29/06/2023): correctif bug du write avec le string en params (variable inacessible)
- v2.10.0 (26/06/2023): ajout de la reconnexion dans le driver de base
- v2.9.5 (26/06/2023): verification du file descripteur dans la liaison TCP
- v2.9.4 (08/06/2023): correctif bug parsing nmea trame
- v2.9.3 (19/05/2023): correctif bug gestion reconnexion
- v2.9.2 (18/05/2023): ajout du timeout dans la liaison TCP + gestion reconnexion
- v2.9.1 (17/05/2023): Correctif lecture par bloc dans la liaison TCP
- v2.9.0 (16/05/2023): Remplacement de la gestion des timeouts (getoftime -> chrono) et correctif de bug de timeout
- v2.8.10 (10/10/2022): Correctif du bug du blocage de la fonction getParameters quand le fichier n'est pas présent 
- v2.8.9 (04/10/2022): Gestion de la création d'un serveur UDP en ajoutant ":-S" apres le port lors de la connexion 
- v2.8.8 (22/09/2022): Correctif bug mineur + compilation avec cmake : ok
- v2.8.7 (16/09/2022): Correctif bug de lecture (fonction readDatasToString(...) dans la classe communication) 
- v2.8.6 (09/08/2022): AJout des fonctions de navigation dans le rejeux 
- v2.8.5 (08/08/2022): Correctif rejeux fichier 
- v2.8.4 (02/08/2022): Ajout de l'entete dans les fichiers de log
- v2.8.3 (28/07/2022): Gestion de la reconnexion en TCP (non implémenté en UDP / SERIAL)
- v2.8.2 (25/07/2022): Correctif bug de lecture (fonction readDatasToString(...))
- v2.8.1 (25/07/2022): Correctif bug d'écriture en tcp et udp (fonction writeDatas(string))
- v2.8.0 (12/07/2022): ajout de la connexion à un serveur HIL TCP
- v2.7.3 (25/05/2019): ajout de prise du repère marin dans les opérations GPS
- v2.7.2 (25/04/2019): Modification de la méthode de lecture dans la communication TCP
- v2.7.1 (14/02/2019):Correction du probème de conversion pour l'utilisation des cos et sin dans GPSTYPE
- v.2.7 (13/02/2019): Creation d'une fonction permettant d'augmenter le temps t'attente entre les caractères
- v.2.6.1 (06/02/2019): Ajout de fonction pour la conversion en cartesien et la mesure de distance entre 2 points GPS
- v.2.6 (05/12/2018): Ajout de nouvelles fonctions de lecture
- v.2.5.3 : Ajout de la fonction pour l'ajout d'un offset sur une position GPS
- v.2.5.2 : Correction bug fabs dans GPSType.h
- v.2.5.1 : Correction mineur dans le readToString 
- v.2.5 : Ajout de la fonction read avec un caractère de début et de fin 
- v.2.4 : Ajout d’un accesseur sur le descripteur de fichier dans la communication
- v.2.3 : Ajout des fonctions de conversion des données GPS
- v.2.2 : Ajout d'une variable endReceived pour dater l'instant de réception de la trame (A.G.) => Activable/Désactivable en (dé)commentant le #define _DEBUG_TIMING dans BaseDriver.h
- v.2.1 : Ajout de fonction dans frameshift pour la conversion Rad<->deg et wrapAngle entre 0 et 2*pi
- v.2.0 : Ajout de la gestion des trames NMEA
- v.1.9 : Ajout du #define COMMUNICATION_TIMEOUT qui est la valeur retournée lors du timeout sur une lecture ainsi que du callback timeout()
- v.1.8 : Ajout d’un constructeur dans le frameshift quaternion et ajout de l’écriture d’une structure par la communication
- v.1.7.2 : Correctif bug avec le OK de la fonction readDatasToString
- v.1.7 : Ajout de timeout dans les lectures comm + ajout du retour d’un string vide si time out ou erreur dans la lecture 
- v.1.6 : Ajout du changement de repère par les quaternions
- v.1.5 : Ajout de la prise en compte des angles dans le changement de repère
- v.1.4 : Correction bug sur la liaison série
- v.1.3 : Correction bug de compilation sous linux
- v.1.2 : Ajout des différentes méthodes de connexion (UDP,TCP,SERIAL,FILE)
- v.1.1 : Correction du bug de la connexion UDP
- v.1.0.1 : récupération d'un timestamp à la réception des trames
- v.1.0 : First version
