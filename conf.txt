#type de connexion SERIAL , UDP , TCP
TYPE_COMM = SERIAL

#information sur la connexion
INFO_CONN = /dev/ttyUSB0:115200

% Activation du log
ENABLE_LOG = 1

#RangeScale (in m)
RANGE_SCALE = 10
