/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 8 septembre 2016, 14:13
 */

#include <cstdlib>
#include <iostream>

#include "TcpComm.h"
#include "UdpComm.h"
#include "SerialComm.h"
#include "BaseDriver.h"
#include "ReadParameters.h"
#include "Frameshift.h"
#include "HILComm.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {


//    BaseDriver dr("test.txt");

//    cout << "connected : "<< dr.connectHIL("127.0.0.1:5556") << endl;

//    for(int i = 0 ; i < 50 ; i++){
//        char msg[5];
//        cout << "time:"<< dr.getLastReceptionTime() << " mess:" << dr.read()<< endl;
//        dr.printToLogFile(msg);
//    }


//    return 0;



//#########################################
//    UdpComm conn;
//    conn.connection("192.168.1.253:4000");
    
//    for(int i = 0 ; i < 50 ; i++){
//        conn.writeDatas("1110");
//        //cout << "mess:" << conn.readDatasToString(1024) << endl;
//        char msg[5];
//        cout << "nb_read" << conn.readDatas(msg,4,2) <<endl;
//        cout << "mess:" << msg << endl;
//        sleep(1);
//    }
    
//    return 0;
    
//#########################################   
//    TcpComm conn;
//    conn.connection("192.168.1.100:1470");
//    
//    for(int i = 0 ; i < 50 ; i++){
//        conn.writeDatas("1110");
//        //cout << "mess:" << conn.readDatasToString(1024) << endl;
//        char msg[5];
//        cout << "nb_read" << conn.readDatas(msg,4,2) <<endl;
//        cout << "mess:" << msg << endl;
//        sleep(1);
//    }
//    
//    return 0;
    
//#########################################    
//    SerialComm conn;
//    cout << "conn :" << conn.connection("/dev/cu.usbserial-AH01GXWG:115200") << endl;
//    
//    for(int i = 0 ; i < 50 ; i++){
//        conn.writeDatas("1110");
//        int status = 0;
//        char msg[5];
//        cout << "nb_read" << conn.readDatas(msg,4) <<endl;
//        cout << "mess:" << msg << endl;
//        //cout << "status:" << status << endl;
//        
//        sleep(1);
//    }
//    
//    return 0;
//#########################################    
//    BaseDriver dr("test.txt","conf.txt");
//    //dr.connect(BaseDriver::SERIAL,"/dev/cu.usbserial-AH01GXWG:115200");
//    dr.connect(BaseDriver::SERIAL,"/dev/cu.usbserial-FTXQW26D:115200");
//    
//    for(int i = 0 ; i < 50 ; i++){
//        dr.writeDatas("1110");
//        char msg[5];
//        cout << "nb_read" << dr.readDatas(msg,4) <<endl;
//        cout << "mess:" << msg << endl;
//        dr.printToLogFile(msg);
//        //cout << "status:" << status << endl;
//        
//        sleep(1);
//    }
//#########################################    
//    ReadParameters readParams("conf.txt");
//    Parameters params = readParams.getParameters();
//    
//    cout<<params.toString()<<endl;
//    
//    bool isPresent = false;
//    cout<<params.getIntParameter("RANGE_SCALE",isPresent)<< " present ?: "<<  isPresent<<endl;
//    
//    
//    return 0;
//######################################### test lecture fichier
//    
//    BaseDriver dr("test.txt");
//    cout << "connected : "<< dr.connectWithConfigFile("datas_seapilot.txt:10") << endl;
// 
//    for(int i = 0 ; i < 50 ; i++){
//        char msg[5];
//        cout << "time:"<< dr.getLastReceptionTime() << " mess:" << dr.readDatasToString('\n')<< endl;
//        dr.printToLogFile(msg);
//    }  
//    
//    
//    
//######################test frameshift
//    double phi = 0.0, theta = 0.0, psi = 180.0; 
//    Frameshift shift(phi, theta, psi);
//    
//    Vector3D vect = shift.shifter(1.0,1.0,1.0);
//    cout << "x:"<< vect.x << " ,y:"<< vect.y << " ,z:"<< vect.z << endl;
//    
//    NormalizedAngle vectAngle = shift.normalizeAngle(0,0,270);
//    cout << "phi:"<< vectAngle.phi << " ,theta:"<< vectAngle.theta << " ,psi:"<< vectAngle.psi << endl;
//
//    
//####
//######################test UDP   
//    BaseDriver dr("test.txt");
//    cout << "connected : "<< dr.connectUDP("192.168.1.34:5000") << endl;
//
//    for(int i = 0 ; i < 50 ; i++){
//        dr.writeDatas("coucou");
//        sleep(1);
//    }
    
}

