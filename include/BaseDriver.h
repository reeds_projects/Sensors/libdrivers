/* 
 * File:   baseDriver.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 8 septembre 2016, 11:50
 */

#ifndef BASEDRIVER_H
#define BASEDRIVER_H

#include <string>
#include <iostream>
#include <fstream>
#include <chrono>
#include "Communication.h"
#include "ReadParameters.h"
#include "Frameshift.h"
#include "TypeDefinition.h"
#include "FileSplitter.h"
#include "Nmea_msg.h"
#include <thread>
#include <algorithm>

#define LOG_FILE_SIZE 100000 // nombre de ligne par fichier
#define SUCCESS 0

// permet de compiler l'exemple de test
//#define MAIN_APP

//#define _DEBUG_TIMING // Ajout de la date de reception pour les tests de durée d'émission.

using namespace std;

class BaseDriver {
public:
    /**
     * Type de connection
     */
    enum Type_comm{
        SERIAL, // liaison série
        UDP,  // socket UDP
        TCP, // socket TCP
        FILE, // fichier;
        HIL, // HIL TCP;
    };
    
    /**
     * Constructeur
     * @param logPathFile, Emplacement du fichier de log sans l'extension
     */
    BaseDriver(string logPathFile);
    
    /**
     * Destructeur
     */
    virtual ~BaseDriver();

    /**
     * Connexion par TCP
     * @param sensorDevPath, Adresse et port du capteur (exemple:"192.168.1.102:1470")
     * @return 0 si ok
     */
    int connectTCP(const char * sensorDevPath);
    
    /**
     * Connexion par UDP
     * @param sensorDevPath, Adresse et port du capteur (exemple:"192.168.1.102:1470")
     * @return 0 si ok
     */
    int connectUDP(const char * sensorDevPath);
    
    /**
     * Connexion par liaison série
     * @param sensorDevPath, Port et vitesse de la liaison (exemple:"/dev/ttyUSB0:115200")
     * @return si le retour (numéro de ) > 0 c'est ok
     */
    int connectSerial(const char * sensorDevPath);
    
    /**
     * Connexion avec un fichier de log pour le rejeux
     * @param file, l'emplacement du fichier de rejeux et la frequence (exemple:"logSeapilot.txt:1" )
     * @return value > 0 si ok;
     */
    int connectFile(const char * file);

    /**
     * Connexion avec un serveur HIL TCP
     * @param hilDevPath, Adresse et port du capteur (exemple:"192.168.1.102:1470")
     * @return value > 0 si ok;
     */
    int connectHIL(const char * hilDevPath);
    
    /**
     * Connexion à partir d'un fichier de configuration
     * @param configFile, Fichier de configuration
     * @return 0 si ok
     */
    int connectWithConfigFile(string configFile);

    /**
     * reconnexion à partir des dernieres infos de configuration
     * @return 0 si ok
     */
    int reconnect();
    
    /**
     * Permet de déconnecter la communication
     */
    void disconnect();
    
    /**
     * Permet lancer l'enregistrement dans le fichier de log
     * @param data , donnée à enregistrer
     */
    void printToLogFile(string data);
    
    /**
     * Permet d'écrire les données sur la liaison sélectionnée lors de la connexion
     * @param buff, données à écrire
     * @param length, nombre de donnée à écrire
     * @return si < 0 alors, il s'agit d'une erreur
     */
    int writeDatas(unsigned char *buff,int length);
    
    /**
     * Permet d'écrire les données sur la liaison sélectionnée lors de la connexion
     * @param datas, structure à écrire
     * @param sizeDatas, nombre de donnée à écrire
     * @return si < 0 alors, il s'agit d'une erreur
     */
    int writeDatas(void *datas,int sizeDatas);
    
    /**
     * Permet d'écrire sur la liaison sélectionnée lors de la connexion
     * @param data, données à transférer
     * @return si < 0 , il s'agit d'une erreur
     */
    int writeDatas(string data);
    
    /**
     * Permet d'écrire au format NMEA sur la liaison sélectionnée lors de la connexion
     * @param data, données à transférer
     * @return si < 0 , il s'agit d'une erreur
     */
    int writeDatas(NMEA_msg msg);
    
    /**
     * Permet de lire les données sur la liaison avec un caractère de début de trame
     * @param charStart, caractère de début de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si < 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,char *buff,int length);
    
    /**
     * Permet de lire les données sur la liaison
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff,int length);
    
    /**
     * Permet de lire les données sur la liaison
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff,int length, int timeout);
    
    /**
     * Permet de lire les données sur la liaison
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout);
    
        /**
     * Permet de lire les données sur la liaison
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff);
    
    /**
     * Permet de lire les données et de faire la conversion en String
     * @param length , longeur de la trame à lire
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    string readDatasToString(int length,int *ok=0 );
    
    /**
     * Permet de lire les données jusqu'à trouver le caractère passé en paramètre
     *  et de faire la conversion en String
     * @param charactere, le caractère de fin 
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    string readDatasToString(char charactere,int *ok=0 );
    
    /**
     * Permet de lire les données avec un timeout et de faire la conversion en String
     * @param length , longeur de la trame à lire
     * @param timeout , timeout en ms
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    string readDatasToString(int length, int timeout,int *ok=0 );
    
    /**
     * Permet de lire les données jusqu'à trouver le caractère passé en paramètre
     *  et de faire la conversion en String
     * @param charactere, le caractère de fin
     * @param timeout , timeout en ms
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    string readDatasToString(char charactere, int timeout,int *ok=0 );
    
    /**
     * Permet de lire les données jusqu'à trouver les caractères passé en paramètre
     *  et de faire la conversion en String
     * @param charactereStart, le caractère de début
     * @param charactereEnd, le caractère de fin
     * @param timeout , timeout en ms
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    string readDatasToString(char charactereStart,char charactereEnd, int timeout,int *ok=0 );
    
    /**
     * Permet d'activer le logging des données à chaque lecture
     * @param enable, true si activer
     */
    void enableLogging(bool enable);
    
    /**
     * Permet de savoir si le loggind est activé
     * @return true si activé
     */
    bool logActived();
    
    /**
     * Permet de retourner l'heure de la dernière réception de trame depuis la connexion
     * @return l'heure de la dernière réception de trame (s) 
     */
    double getLastReceptionTime();
    
    /**
     * Permet de retourner le type de communication utilisé par le drivers
     * @return le type de communication (SERIAL,TCP,UDP,FILE)
     */
    BaseDriver::Type_comm getCommunicationType();
    
    /**
     * Permet de lire les données 
     * Ca lance séquentiellement les callbacks suivantes 
     * - waitDatas()
     * - readOnSensor() si l'on est dans une communication SERIAL, TCP ou UDP
     * - readOnFile() si l'on lit un fichier
     * - readOnHILServer() si l'on lit un server HIL
     * @return 0 si ok 
     */
    virtual int read();
    
    /**
     * Permet d'envoyer des commandes
     * @param cmd, la commande
     * @param size, la taille de la commande
     * @return 0 si ok
     */
    virtual int write(unsigned char * cmd , int size);
    
    /**
     * Permet d'envoyer une structure
     * @param datas, la structure
     * @param sizeDatas, la taille de la commande
     * @return 0 si ok
     */
    virtual int write( void *datas, int sizeDatas );
    
    /**
     * Permet de définir l'orientation du capteur dans le repère de l'engin
     * @param phi, angle autour de l'axe x (deg)
     * @param theta, angle autour de l'axe y (deg)
     * @param psi, angle autour de l'axe z (deg)
     */
    void setSensorOrientation(double phi, double theta, double psi);
    
    /**
     * Permet de retourner le repère de l'engin
     * @return repère de l'engin
     */
    Frameshift getSensorOrientation();
    
    /**
     * Permet de savoir si l'on est en comminucation TCP
     * @return true si c'est la communication TCP qui est utilisée
     */
    bool isTCPCommunication();
    
    /**
     * Permet de savoir si l'on est en comminucation UDP
     * @return true si c'est la communication UDP qui est utilisée
     */
    bool isUDPCommunication();
    
    /**
     * Permet de savoir si l'on est en comminucation SERIAL
     * @return true si c'est la communication SERIAL qui est utilisée
     */
    bool isSERIALCommunication();
    
    /**
     * Permet de savoir si l'on est en comminucation FILE
     * @return true si c'est la communication FILE qui est utilisée
     */
    bool isFILECommunication();
    
    /**
     * Permet de retourner la communication
     * @return la communication
     */
    Communication * getCommunication();
    
    /**
     * Permet d'affecter la communication
     * @param comm, la communication à affecter
     */
    void setCommunication(Communication * comm);
    
    /**
     * Permet de récupérer l'horloge système
     * @return l'horloge système en ms du système
     */
    long int getTimeSystem();

    /**
     * Permet de retourner le status de la connexion
     * @return true si connecté
     */
    bool isConnected();

    /**
     * Permet d'affecter un nom au driver
     * @param driverName, nom du driver
     */
    void setDriverName(string driverName);

    /**
     * Permet de retourner le nom du driver
     * @return le nom du driver
     */
    string getDriverName();

    /**
     * Permet d'affecter une description au driver
     * @param description, description du driver
     */
    void setDriverDescription(string description);

    /**
     * Permet de retourner la description du driver
     * @return description du driver
     */
    string getDriverDescription();

    /**
     * Permet d'affecter le numéro de version du driver
     * @param major, numero de version majeur
     * @param minor, numero de version mineur
     */
    void setDriverVersion(int major, int minor);

    /**
     * Permet de retourner le numéro de version
     * @return le numéro de version sous la forme "1.3"
     */
    string getDriverVersion();

    /**
     * Permet de retourner le nombre de ligne utilisé pour l'entete du fichier de log
     * @return le nombre de ligne
     */
    int getNumberOfLineForHeader();

    
protected:
    /**
     * Permet de lancer la connexion
     * @param typeComm , le type de connexion (SERIAL, UDP, TCP)
     * @param infoConnection, - dans le cas d'une liaison série il s'agit du port et du baudrate (ex: /dev/ttyUSB0:115200 )
     * - pour UDP,TCP, HIL (UDP) , il s'agit de l'adresse IP du serveur et le port(ex: 192.168.1.100:1470 )
     * @return si < 0, il s'agit d'une erreur
     */
    int connect(Type_comm typeComm,string infoConnection);
    
    /**
     * Permet de lancer la connexion avec les données du fichier de configuration
     * clés nécessaires dans le fichier
     * TYPE_COMM = SERIAL OU UDP OU TCP OU HIL
     * INFO_CONN = 192.168.1.100:1470 par exemple pour TCP 
     * @param configFile , emplacement du fichier
     * @return si < 0, il s'agit d'une erreur
     */
    int connect(string configFile);

    /**
     *
     *
     */
    /**
     * Callback appeler lors de la perte de connexion (CONNECTION_RESET_BY_PEER est retourné par waitDatas())
     * @param reconn_result, retour de la fonction de reconnexion (si < 0, il s'agit d'une erreur )
     */
    virtual void connectionLost( int reconn_result);
    
    /**
     * Callback appeler après la connexion 
     * @return 0 si ok 
     */
    virtual int initialize();
    
    /**
     * Callback appelé lorsque la connexion depuis un fichier de configuration est réalisé
     * @param params, liste des paramètres du fichier de configuration
     * @return 0 si ok
     */
    virtual int initializeWithParameters(Parameters params);
    
 #ifndef MAIN_APP 
    /**
     * Callback pour le nom des variables du log csv
     * @return une chaine de caractère sous la forme "Timestamp;Vbat;CBat"
     */
    virtual string logHeadersVarNames(){return "";};

    /**
     * Callback pour l'unité des variables du log csv
     * @return une chaine de caractère sous la forme "ms;V;A "
     */
    virtual string logHeadersVarUnits(){return "";};
    /**
     * Callback permettant d'attendre les caractères et ainsi créer la trame 
     * afin de la parser dans readOnSensor() ou readOnFile();
     * @return la trame à parser
     */
  
    virtual string waitDatas() = 0; 
    /**
     * Callback pour parser les données venant d'une communication autre que d'un fichier
     * @param trame, la trame à parser
     * @return 0 si ok 
     */
    virtual int readOnSensor(string trame) = 0;
    /**
     * Callback pour parser les données venant d'un fichier de log
     * @param trame, la trame à parser
     * @return 0 si ok 
     */
    virtual int readOnFile(string trame) = 0;

    /**
     * Callback pour parser les données venant d'un serveur HIL
     * @param trame, la trame à parser
     * @return 0 si ok
     */
    virtual int readOnHILServer(string trame)=0;
    
    /**
     * Callback lancer lorsque COMMUNICATION_TIMEOUT est retourné par waitDatas()
     */
    virtual void timeout() = 0;

    /**
     * Callback lancer lors de la reconnexion
     */
    virtual void reconnected() = 0;
    
#endif     
    /**
     * Permet de passer une donnée en xyz du repère capteur au repère engin
     * @param x, position en x dans le repère capteur
     * @param y, position en y dans le repère capteur
     * @param z, position en z dans le repère capteur
     * @return position de la donnée en xyz dans le repère de l'engin
     */
    Vector3D frameshiftLocal(double x, double y , double z);
    
    /**
     * Permet de passer les angles du repère capteur au repère engin
     * @param phi, angle autour de x dans le repère capteur
     * @param theta, angle autour de y dans le repère capteur
     * @param psi, angle autour de z dans le repère capteur
     * @return angles des capteurs dans le repère de l'engin
     */
    NormalizedAngle normalizeAngleLocal(double phi, double theta , double psi);
    

#ifdef _DEBUG_TIMING
        std::chrono::high_resolution_clock::time_point _endReceived;
#endif
    
private:
    Communication *_comm;
    string _logPathFile;
    string _confPathFile;
    dwf_utils::FileSplitter _file;
    bool _logActived;
    Type_comm _commType;
    string _driverName;
    string _driverDescription;
    string _driverVersion;
    int _numberLineHeader;
    string _lastConnectionInfo;
    
    // heure de de connection
    chrono::time_point<chrono::system_clock> _startApp;
    double _timeLastReception;
    
    //variable du repère engin
    Frameshift _frameshiftLocal;

    //bool _isConnected;

    //gestion du thread de rejeux des fichiers
    thread *_replayFileThread = nullptr;
    bool _replayFileThread_stop = true;
    bool _replayFileThread_pause = false;
    
    

};

#endif /* BASEDRIVER_H */

