/* 
 * File:   Communication.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 9 septembre 2016, 13:51
 */

#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <chrono>
#include <iostream>

#define COMMUNICATION_TIMEOUT "communication_timeout"
#define COMMUNICATION_ERROR "communication_error"
#define CONNECTION_RESET_BY_PEER "connection_reset_by_peer"
#define CONNECTION_NOT_CONNECTED "connection_not_connected"
#define RECEIVE_FRAME_SIZE_NOT_CORRECT "receive_frame_size_not_correct"
#define EMPTY_FRAME "empty"

using namespace std;

class Communication {
public:
    /**
     * Constructeur
     */
    Communication(){
       _fileDescriptor = -1; 
       _isConnected=false;
    };
    
    /**
     * Destructeur
     */
    virtual ~Communication(){
        if (_fileDescriptor > 0) disconnect();
    };

    /**
     * Permet de lancer la communication
     * @param , le paramètre dépend du type de communication
     * @return , return 0 si ok 
     */
    virtual int connection(string) = 0;
    
    /**
     * Permet de lire les données et de faire la conversion en String
     * @param length , longeur de la trame à lire
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    virtual string readDatasToString(int length,int *ok=0){
        char buff[length];
        if(readDatas(buff,length) < 0){
            printf("error read datas");
            if(ok != NULL)*ok = -1;
            return "";
        }
        if(ok != NULL)*ok = 0;
        
        string reply = buff;
        return reply;
    };
    
    /**
     * Permet de lire les données jusqu'à trouver le caractère passé en paramètre
     *  et de faire la conversion en String
     * @param charactere, le caractère de fin 
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    virtual string readDatasToString(char charactere,int *ok=0){
        char recvbuf[1];
        string reply = "";
        do {

            if (readDatas(recvbuf, 1) < 0) {
                printf("Error reading data from the device.\n");
                if(ok != NULL)*ok = -1;
                return "";
            }
            reply += recvbuf[0];

            //printf("char:%c\n",recvbuf[0]);

        } while (recvbuf[0] != charactere);
        if(ok != NULL)*ok = 0;
        return reply;
    };
    
    /**
     * Permet de lire les données avec un timeout et de faire la conversion en String
     * @param length , longeur de la trame à lire
     * @param timeout, temps en ms du timeout
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    virtual string readDatasToString(int length,int timeout,int *ok=0){
        //printf("BaseDriver :: Communication :: readDatasToString(%d,%d) \n",length,timeout);
        char buff[length];
        int size = 0;
        if( (size = readDatas(buff,length,timeout)) < 0){
            printf("BaseDriver :: Communication :: readDatasToString() -- error!! read Data \n");
            if(ok != NULL)*ok = size;
            if(size == -3)return CONNECTION_RESET_BY_PEER;
            else if(size == -2)return COMMUNICATION_TIMEOUT;
            else return COMMUNICATION_ERROR;
        }
        if(size != length){
            printf("BaseDriver :: Communication :: readDatasToString() -- error!! received frame size not correct :  size received :%d != %d\n",size,length);
            if(ok != NULL)*ok = -2;
            return RECEIVE_FRAME_SIZE_NOT_CORRECT;
        }
        if(ok != NULL)*ok = 0;

        //printf("BaseDriver :: Communication :: readDatasToString() -- charactere size: %d \n",size);
        string reply = string(buff,size);
        //printf("BaseDriver :: Communication :: readDatasToString() -- string size: %d \n",reply.size());
        return reply;
    };
    
    /**
     * Permet de lire les données jusqu'à trouver le caractère passé en paramètre avec un timeout
     *  et de faire la conversion en String
     * @param charactere, le caractère de fin 
     * @param timeout, temps en ms du timeout
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    virtual string readDatasToString(char charactere,int timeout,int *ok=0){
        char recvbuf[1];
        string reply = "";
        int ret = 0;
        do {

            if ( (ret = readDatas(recvbuf, 1,timeout)) < 0) {
                cout << "BaseDriver :: Communication :: readDatasToString() --Error reading data from the device." <<endl;
                if(ok != NULL)*ok = ret;

                if(ret == -3)return CONNECTION_RESET_BY_PEER;
                else if(ret == -2)return COMMUNICATION_TIMEOUT;
                else return COMMUNICATION_ERROR;
            }
            reply += recvbuf[0];

            //std::cout << "char:"<< recvbuf[0] <<std::endl;

        } while (recvbuf[0] != charactere);
        if(ok != NULL)*ok = 0;
        
        return reply;
    };
    
    /**
     * Permet de lire les données jusqu'à trouver les caractères passé en paramètre avec un timeout
     *  et de faire la conversion en String
     * @param charactereStart, le caractère de début
     * @param charactereEnd, le caractère de fin
     * @param timeout, temps en ms du timeout
     * @param ok , 0 si ok
     * @return la chaine de caractère lu
     */
    virtual string readDatasToString(char charactereStart,char charactereEnd,int timeout,int *ok=0){
        char buff[2048];
        int size_buff = 0;
        if( (size_buff = readDatas(charactereStart,charactereEnd,buff,timeout)) < 0){
            printf("BaseDriver :: Communication :: readDatasToString() -- error read datas");
            if(ok != NULL)*ok = size_buff;
            if(size_buff == -3)return CONNECTION_RESET_BY_PEER;
            else if(size_buff == -2)return COMMUNICATION_TIMEOUT;
            else return COMMUNICATION_ERROR;
        }
        if(ok != NULL)*ok = 0;
        
        string reply = string(buff,size_buff);
        return reply;
    };
    
    /**
     * Permet de lire les données sur le lien de communication avec un caractère de début de trame
     * @param charStart, caractère de début de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si < 0 , il s'agit d'erreur
     */
    virtual int readDatas(unsigned char /*charStart*/,char */*buff*/,int /*length*/){
        printf("readDatas(unsigned char charStart, char *buff,int length) undeclared");
        return -1;
    };
    
    /**
     * Permet de lire les données sur le lien de communication
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    virtual int readDatas(char */*buff*/,int /*length*/){
        printf("readDatas(char *buff,int length) undeclared");
        return -1;
    };
    
    /**
     * Permet de lire les données
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    virtual int readDatas(char */*buff*/,int /*length*/, int /*timeout*/){
        printf("readDatas(char *buff,int length, int timeout) undeclared");
        return -1;
    };
    
    /**
     * Permet de lire les données
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    virtual int readDatas(unsigned char /*charStart*/,unsigned char /*charEnd*/,char */*buff*/, int /*timeout*/){
        printf("readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout) undeclared");
        return -1;
    };
    
    /**
     * Permet de lire les données
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    virtual int readDatas(unsigned char /*charStart*/,unsigned char /*charEnd*/,char */*buff*/){
        printf("readDatas(unsigned char charStart,unsigned char charEnd,char *buff) undeclared");
        return -1;
    };
    
    /**
     * Permet d'écrire les données sur le lien de communication
     * @param buff, données à écrire
     * @param length, nombre de donnée à écrire
     * @return si < 0 alors, il s'agit d'une erreur
     */
    int writeDatas(char *buff,int length){
        return  writeDatas((void*)buff,length);
    }
    
    /**
     * Permet d'écrire les données sur le lien de communication
     * @param data, données à écrire
     * @return si < 0 alors, il s'agit d'une erreur
     */
    virtual int writeDatas(const std::string &/*data*/){
        printf("writeDatas(string data) undeclared\n");
        return -1;
    }
    
    virtual int writeDatas( void */*datas*/, int /*sizeDatas*/ ){
        printf("writeDatas(void *datas, int sizeDatas) undeclared\n");
        return -1;
    }

    /**
     * Callback appeler lors de la perte de connexion (CONNECTION_RESET_BY_PEER est retourné par waitDatas())
     * @return si < 0 alors, il s'agit d'une erreur
     */
    virtual int connectionLost(){
        printf("connectionLost() undeclared\n");
        return -1;
    };

    /**
     * Callback appeler lors de la recupération de connexion
     * @return si < 0 alors, il s'agit d'une erreur
     */
    virtual int connectionReconnected(){
        printf("connectionReconnected() undeclared\n");
        return -1;
    };
    
    /**
     * Permet de faire la déconnexion
     */
    virtual void disconnect(){};
    
    /**
     * Permet de retourner le descripteur de fichier
     * @return le file descriptor
     */
    int getFileDescriptor(){return _fileDescriptor;};

    /**
     * Permet de vérifier la validité du descripteur de fichier
     * @return
     */
    int fileDescriptorIsValid(){return fcntl(_fileDescriptor, F_GETFL) != -1 || errno != EBADF;}

    /**
     * Permet de retourner le status de la connexion
     * @return true si connecté
     */
    bool isConnected(){return _isConnected;}

private:
    
protected:
    // descripteur de fichier
    int _fileDescriptor;
    // timeout
    std::chrono::time_point<std::chrono::system_clock> _timeout;

    bool _isConnected;

};

#endif /* COMMUNICATION_H */

