/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileComm.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 2 novembre 2016, 15:08
 */

#ifndef FILECOMM_H
#define FILECOMM_H

#include <string>
#include <chrono>
#include <vector>

#include "Communication.h"

#define FILE_OK             1
#define FILE_ERR            -1
#define FILE_ERR_TIMOUT     -2
#define FILE_ERR_FRAME      -3
#define SIZE_NAME_PORT      80	

class FileComm : public Communication{
public:
    /**
     * Constructeur
     */
    FileComm();
    
    /**
     * destructeur
     */
    virtual ~FileComm();
    
    /**
     * Permet d'ouvrir un fichier de log pour le rejeux de donnée
     * @param path, emplacement du fichier de log
     * @return -1 si erreur
     */
    int connection(string path);
    
    /**
     * Permet de lire les données du fichier de log
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff,int length);
    
    /**
     * Permet de lire les données du fichier de log
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff, int length, int timeout_ms);
    
    /**
     * Permet de lire les données du fichier de log
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout_ms);
    
    /**
     * Permet de lire les données du fichier de log
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff);
    
    /**
     * Permet de lire les données et de faire la conversion en String
     * @param length , longeur de la trame à lire
     * @return la chaine de caractère lu
     */
    string readDatasToString(int length,int *ok=0 );
    
    /**
     * Permet de lire les données jusqu'à trouver le caractère passé en paramètre
     *  et de faire la conversion en String
     * @param charactere, le caractère de fin 
     * @return la chaine de caractère lu
     */
    string readDatasToString(char charactere,int *ok=0);
    
    /**
     * Permet d'écire des données du fichier de log
     * @param data, chaine de caractère à envoyer sur la socket
     * @return 0 si ok, -1 si erreur
     */
    int writeDatas(const std::string & data);

    /**
     * Permet d'envoyer une structure sur la socket TCP
     * @param datas, la structure
     * @param sizeDatas, la taille de la commande
     * @return 0 si ok
     */
    int writeDatas( void *datas, int sizeDatas );

    
     /**
     * Permet de faire la déconnexion 
     */
    void disconnect();

    /**
     * Permet de retourner la ligne au numéro de ligne indiqué
     * @param lineNumber, numéro de ligne
     * @retour la ligne
     */
    string getLineAt(unsigned int lineNumber, int *ok = nullptr);

    /**
     * Permet de récuperer la ligne suivante du fichier
     * @return la ligne suivante
     */
    string nextLine(int *ok= nullptr);

    /**
     * Permet de récuperer la ligne précédente du fichier
     * @return la ligne précédente
     */
    string previousLine(int *ok= nullptr);

    /**
     * Permet de récuperer la ligne courante du fichier
     * @return la ligne courante
     */
    string currentLine(int *ok= nullptr);

    /**
     * Permet de retourner le numéro de la ligne courante
     * @return le numéro de la ligne courante
     */
    unsigned int currentLineNumber();

    /**
     * Permet de retourner le nombre de ligne dans le fichier
     * @return le nombre de ligne
     */
    unsigned int getNumberOfLine();

    /**
     * permet de retourner le timestamp de la ligne courante
     * @return le timestamp de la ligne courante
     */
    unsigned long getCurrentLineTimestamp();

    /**
     * Permet de retourner le timestamp de la ligne suivante
     * @return le timestamp de la ligne suivante
     */
    unsigned long getNextLineTimestamp();

    /**
     * Permet de retourner le timestamp de la ligne précédente
     * @return le timestamp de la ligne précédente
     */
    unsigned long getPreviousLineTimestamp();


    
private:
    unsigned int _period_us;
    std::vector<int> _lineTable;
    unsigned long _currentIndexLine;

    std::vector<unsigned long> _lineTimestampTable;

};

#endif /* FILECOMM_H */
