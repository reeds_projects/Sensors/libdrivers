/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Frameshift_quaternion.h
 * Author: ropars.benoit
 *
 * Created on 13 mars 2017, 14:36
 */

#ifndef FRAMESHIFT_QUATERNION_H
#define FRAMESHIFT_QUATERNION_H

#include <eigen3/Eigen/Geometry>
#include "TypeDefinition.h"

using namespace Eigen;


/**
 * Permet de faire les changement de repère
 */
class Frameshift_quaternion {
public:
    
    /**
     * Constructeur par default
     */
    Frameshift_quaternion() 
    : _Q_rot(Quaterniond(1,0,0,0)){}
    
    /**
     * Constructeur pour les angle en degré
     * @param phi_deg, angle de rotation suivant l'axe x (degré)
     * @param theta_deg, angle de rotation suivant l'axe y (degré)
     * @param psi_deg, angle de rotation suivant l'axe z (degré)
     */
    Frameshift_quaternion(double phi_deg, double theta_deg, double psi_deg) 
    : _Q_rot(euler2Quaternion(degTorad(phi_deg),degTorad(theta_deg),degTorad(psi_deg))){}

    /**
     * Constructeur pour un quaternion donné
     * 
     */
    Frameshift_quaternion(double w, double x, double y, double z) 
    : _Q_rot(Quaterniond(w,x,y,z)){}
    
    /**
     * Constructeur pour un quaternion donné
     * 
     */
    Frameshift_quaternion(Quaterniond quat) 
    : _Q_rot(quat){}
    
    /**
     * Destructeur
     */
    virtual ~Frameshift_quaternion(){};
    
    /**
     * Permet de convertir les angles d'euler en quaternion
     * @param roll_rad, angle de rotation suivant l'axe x (rad)
     * @param pitch_rad, angle de rotation suivant l'axe y (rad)
     * @param yaw_rad, angle de rotation suivant l'axe z (rad)
     * 
     */
    static Eigen::Quaterniond euler2Quaternion(const double roll_rad, const double pitch_rad, const double yaw_rad){
        
        Quaterniond q;
	double t0 = std::cos(yaw_rad* 0.5);
	double t1 = std::sin(yaw_rad * 0.5);
	double t2 = std::cos(roll_rad * 0.5);
	double t3 = std::sin(roll_rad * 0.5);
	double t4 = std::cos(pitch_rad * 0.5);
	double t5 = std::sin(pitch_rad * 0.5);

	q.w() = t0 * t2 * t4 + t1 * t3 * t5;
	q.x() = t0 * t3 * t4 - t1 * t2 * t5;
	q.y() = t0 * t2 * t5 + t1 * t3 * t4;
	q.z() = t1 * t2 * t4 - t0 * t3 * t5;
	return q;
        
    }   
    
    static Vector3d quaternion2Euler(const Eigen::Quaterniond& q){
        
        Vector3d v;
        double ysqr = q.y() * q.y();

	// roll (x-axis rotation)
	double t0 = +2.0 * (q.w() * q.x() + q.y() * q.z());
	double t1 = +1.0 - 2.0 * (q.x() * q.x() + ysqr);
	v.data()[0] = std::atan2(t0, t1);

	// pitch (y-axis rotation)
	double t2 = +2.0 * (q.w() * q.y() - q.z() * q.x());
	t2 = t2 > 1.0 ? 1.0 : t2;
	t2 = t2 < -1.0 ? -1.0 : t2;
	v.data()[1] = std::asin(t2);

	// yaw (z-axis rotation)
	double t3 = +2.0 * (q.w() * q.z() + q.x() * q.y());
	double t4 = +1.0 - 2.0 * (ysqr + q.z() * q.z());  
	v.data()[2] = std::atan2(t3, t4);
        
        return v;
    }
    
    /**
     * Permet de faire le changement de repère sur un point en 3D
     * @param point, point sur lequel il faut faire la transformation
     * @return retourne le point avec la transformation
     */
    Vector3d shifter(Vector3d vector){
        
        Quaterniond p;
        p.w() = 0;
        p.vec() = vector;
        _Q_rot.normalize();
        Quaterniond rotatedP = _Q_rot.inverse() * p * _Q_rot;
        return rotatedP.vec();
    }
    
    /**
     * Permet de faire le changement de repère sur un point en 3D
     * @param x, coordonnée en x
     * @param y, coordonnée en y
     * @param z, coordonnée en z
     * @return retourne le point avec la transformation
     */
    Vector3d shifter(double x, double y, double z){
        Vector3d vect(x,y,z);
        return shifter(vect);
        
    }
    
    /**
     * Permet de retouner le quaternion de changement de repère
     * @return quaternion de changement de repère
     */
    Quaterniond getQuaternion(){
        return _Q_rot;
    }
    
    /**
     * Permet d'affecter de le quaternion de rotation
     * @param quat, quaternion de rotation
     */
    void setQuaternion(Quaterniond quat){
        _Q_rot = quat;
    }
    
private:
    Quaterniond _Q_rot;// angle de rotation suivant l'axe x (radian)
    
    /**
     * Permet de convertir un angle en radian afin d'obtenir un angle en degré
     * @param angle, angle en radian à convertir
     * @return l'angle en degré
     */
    double radTodeg(double angle){return (double)((180.0 * angle) / PI);}
    
    /**
     * Permet de convertir un angle en degré afin d'obtenir un angle en radian
     * @param angle, angle en degré à convertir
     * @return l'angle en radian
     */
    double degTorad(double angle){return (double)((PI * angle) / 180.0);}
    
};

#endif /* FRAMESHIFT_QUATERNION_H */

