/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GPSType.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 26 octobre 2018, 09:16
 */

#ifndef GPSTYPE_H
#define GPSTYPE_H

#include <cstdio>
#include <math.h>

#include "TypeDefinition.h"

using namespace std;

#define R_EARTH_m 6378137//rayon de la terre en mètre

/**
 * Permet de faire les changement de repère
 */

namespace GPS_conv{
    /**
     * Permet de convertir les données décimal en DMS
     * @param dms, DMS (degrees, minutes, seconds) à convertir en décimal
     * @return les données gps au format décimal (43.63944444) (3.84500000)
     */
    static GPS_dec DMSToDecimal(GPS_dms dms){
        GPS_dec dec;
        int D_lat,M_lat,S_lat,D_long,M_long,S_long;
        char O_lat,O_long;
        
        sscanf(dms.latitude,"%d°%d'%d\"%c",&D_lat,&M_lat,&S_lat,&O_lat);
        sscanf(dms.longitude,"%d°%d'%d\"%c",&D_long,&M_long,&S_long,&O_long);
        
        dec.latitude = D_lat + (M_lat/60.0) + (S_lat/3600.0);
        if(O_lat == 'S')dec.latitude *= -1.0;
        
        dec.longitude = D_long + (M_long/60.0) + (S_long/3600.0);
        if(O_long == 'W')dec.longitude *= -1.0;
        
        return dec;

    }
    
    /**
     * Permet de convertir les données décimal en DMS
     * @param latitude, latitude en DMS (43°38'22"N)
     * @param longitude, longitude en DMS (3°50'42"E)
     * @return les données gps au format décimal (43.63944444) (3.84500000)
     */
    static GPS_dec DMSToDecimal(char * latitude,char * longitude){
        GPS_dms dms={latitude,longitude};
        return DMSToDecimal(dms);

    }
    
    /**
     * Permet de convertir les données décimal en DMS
     * @param dms, DMS (degrees, minutes, seconds) à convertir en décimal
     * @return les données gps au format DMS (degrees, minutes, seconds)
     */
    static GPS_dms DecimalToDMS(GPS_dec dec){
        GPS_dms dms;
        int D_lat = dec.latitude;
        int M_lat = fmod(fabs(dec.latitude)*60.0, 60.0);
        int S_lat = fmod(fabs(dec.latitude)*3600, 60.0);
        char O_lat= D_lat < 0 ? 'S':'N';
        
        int D_long = dec.longitude;
        int M_long = fmod(fabs(dec.longitude)*60.0, 60.0);
        int S_long = fmod(fabs(dec.longitude)*3600, 60.0);
        char O_long= D_long < 0 ? 'W':'E';
        
        asprintf(&dms.latitude,"%d°%d'%d\"%c",D_lat,M_lat,S_lat,O_lat);
        asprintf(&dms.longitude,"%d°%d'%d\"%c",D_long,M_long,S_long,O_long);
        return dms;
    }
    
    /**
     * Permet de convertir les données décimal en DMS
     * @param latitude, latitude en decimal (43.63944444)
     * @param longitude, longitude en decimal (3.84500000)
     * @return les données gps au format DMS (degrees, minutes, seconds)
     */
    static GPS_dms DecimalToDMS(double latitude, double longitude){
        GPS_dec dec={latitude,longitude};
        return DecimalToDMS(dec);
    }
    
}
using namespace GPS_conv;

class GPS_data{
public:
    
    /**
     * Constructeur par default
     */
    GPS_data()
    : _lat(0.0),_long(0.0){}

    /**
     * Constructeur pour la latitude et la longitude en décimal
     * @param latitude, latitude en decimal (43.63944444)
     * @param longitude, longitude en decimal (3.84500000)
     */
    GPS_data(double latitude, double longitude)
    : _lat(latitude),_long(longitude){}
    
    /**
     * Constructeur pour la latitude et la longitude en DMS (degrees, minutes, seconds)
     * @param latitude, latitude en DMS (43°38'22"N)
     * @param longitude, longitude en DMS (3°50'42"E)
     */
    GPS_data(char * latitude,char * longitude){
        GPS_dec dec = GPS_conv::DMSToDecimal(latitude,longitude);
        _lat = dec.latitude;
        _long = dec.longitude;
    }
    
    /**
     * Destructeur
     */
    virtual ~GPS_data(){};
    
    
    /**
     * Permet d'affecter la latitude et la longitude en decimal
     * @param latitude, latitude en decimal (43.63944444)
     * @param longitude, longitude en decimal (3.84500000)
     */
    void setLatLong(double latitude, double longitude){
        _lat = latitude;
        _long = longitude;
    }
    
    /**
     * Permet d'affecter la latitude et la longitude en DMS (degrees, minutes, seconds)
     * @param latitude, latitude en DMS (43°38'22"N)
     * @param longitude, longitude en DMS (3°50'42"E)
     */
    void setLatLong(char * latitude,char * longitude){
        GPS_dec dec = GPS_conv::DMSToDecimal(latitude,longitude);
        _lat = dec.latitude;
        _long =dec.longitude;
    }
    
    /**
     * Permet de retourner les données GPS au format décimal
     * @return les données gps au format décimal (43.63944444) (3.84500000)
     */
    GPS_dec toDecimal(){
        GPS_dec dec = {_lat,_long};
        return dec;
    }
    
    /**
     * Permet de retourner les données GPS au format décimal
     * @return les données gps au format décimal (43.63944444) (3.84500000)
     */
    GPS_dms toDMS(){
        return GPS_conv::DecimalToDMS(_lat,_long);
    }
    
    enum FRAME_ORIENTATION{
        UNDERWATER_FRAME,
        EARTH_FRAME
        
    };
    
    /**
     * Permet d'ajouter un offset à la position gps
     * @param dx, offset en x (m)
     * @param dy, offset en y (m)
     * @param frame, type de repère terrestre ou marin
     * @return la nouvelle position calculer avec l'offset
     */
    GPS_data addOffset(double dx , double dy,FRAME_ORIENTATION frame = UNDERWATER_FRAME){
        
        //changement de repère
        if(frame == UNDERWATER_FRAME){
            double tmp = dx;
            dx = dy;
            dy = tmp;
        }
        
        double lat = _lat + (180/PI)*(dy/R_EARTH_m);
        double lon = _long + (180/PI)*(dx/R_EARTH_m)/cos(_lat* PI/ 180);
        return GPS_data(lat,lon);
    }
    
    /**
     * Permet de convertir la longitude et la latitude en cartesien
     * @param frame, type de repère terrestre ou marin
     * @return les coordonnées xyz en m
     */
    Vector3D toCartesianXYZ(FRAME_ORIENTATION frame = UNDERWATER_FRAME){
        
        Vector3D ret;
        double lat_rad = _lat * PI/ 180;
        double lon_rad = _long * PI/ 180;
        ret.x = R_EARTH_m * cos(lat_rad) * sin(lon_rad);
        ret.y = R_EARTH_m * cos(lat_rad) * cos(lon_rad);
        ret.z = R_EARTH_m * sin(lat_rad);
        
        if(frame == UNDERWATER_FRAME){
            double tmp = ret.x;
            ret.y = ret.x;
            ret.x = tmp;
            ret.z = -ret.z;
        }
        
        return ret;
        
    }
    
    /**
     * Permet de données la distance entre deux points GPS
     * @param secondPoint, Second point GPS pour la mesure de distance
     * @return distance en m
     */
    double distance(GPS_data * secondPoint){
        //
        double lat_1 = getDecimalLatitude();
        double lat_2 = secondPoint->getDecimalLatitude();
        double lon_1 = getDecimalLongitude();
        double lon_2 = secondPoint->getDecimalLongitude();
        double phi_1 =  lat_1 *(PI/ 180);
        double phi_2 = lat_2 *(PI/ 180);
        double delta_phi = (lat_2-lat_1)*(PI/ 180);
        double delta_lambda = (lon_2-lon_1)*(PI/ 180);
        double a = sin(delta_phi/2) * sin(delta_phi/2) + cos(phi_1) * cos(phi_2) * sin(delta_lambda/2) * sin(delta_lambda/2);
        double c = 2 * atan2(sqrt(a), sqrt(1-a));
        return R_EARTH_m * c;
    }

    /**
     * Permet de données le cap entre deux points GPS
     * @param secondPoint, Second point GPS pour la mesure de cap
     * @return cap en °
     */

    double azimut(GPS_data * secondPoint){
        double lat_1 = getDecimalLatitude() * PI/180;
        double lon_1 = getDecimalLongitude() * PI/180;

        double lat_2 = secondPoint->getDecimalLatitude() * PI/180;
        double lon_2 = secondPoint->getDecimalLongitude() * PI/180;

        double x = cos(lat_1)*sin(lat_2)-sin(lat_1)*cos(lat_2)*cos(lon_2-lon_1);
        double y = sin(lon_2-lon_1)*cos(lat_2);

        double azim = 2*atan(y/(sqrt(x*x + y*y)+x)) * 180/PI;

        return azim;
    }
    
    /*
     * Permet de retourner la latitude en décimal
     * @return la donnée la latitude en décimal (43.63944444)
     */
    double getDecimalLatitude(){return _lat;};
    
        /*
     * Permet de retourner la latitude en DMS
     * @return la donnée la latitude en DMS (43°38'22"N)
     */
    char * getDMSLatitude(){return GPS_conv::DecimalToDMS(_lat,_long).latitude;};
    
    /*
     * Permet de retourner la longitude en décimal
     * @return la donnée la longitude en décimal (3.84500000)
     */
    double getDecimalLongitude(){return _long;};
    
    /*
     * Permet de retourner la longitude en DMS
     * @return la donnée la longitude en DMS (3°50'42"E)
     */
    char * getDMSLongitude(){return GPS_conv::DecimalToDMS(_lat,_long).longitude;};
    
private:
    double _lat;// decimal latitude
    double _long;// decimal longitude
    

    
};


#endif /* FRAMESHIFT_H */

