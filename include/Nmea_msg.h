/* 
 * File:   Nmea.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 10 janvier 2018, 13:51
 */

#ifndef NMEA_H
#define NMEA_H

#include <string.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>


typedef std::string NMEA_string;
typedef std::string NMEA_trameType; // exemple "GPGLL"
typedef std::vector<std::string> NMEA_datas; // liste des datas "5300.97914,N,00259.98174,E,125926"


using namespace std;

class NMEA_msg{
public:
    /**
     * Constructeur
     */
    NMEA_msg(NMEA_trameType trameType, NMEA_datas datas){
        _msg = trameType;
        for(int i = 0 ; i < datas.size() ; i++)
            _msg += ","+datas.at(i);
        string checksum = calculateChecksum(_msg);
        
        //formatage du message
        _msg = "$" + _msg + "*" + checksum + "\n";
        
    };
    /**
     * Constructeur
     */
    NMEA_msg(std::string msg){
        _msg = msg;
        string checksum = calculateChecksum(_msg);
        
        //formatage du message
        _msg = "$" + _msg + "*" + checksum + "\n";
        
    };
    
    /**
     * Destructeur
     */
    virtual ~NMEA_msg(){
    };
    
    /**
     * Permet de retourner le message NMEA complet (exemple "$GPGLL,5300.97914,N,00259.98174,E,125926,A*28\n")
     * @return le message NMEA
     */
    std::string toString(){
        return _msg;
    };
    
    /**
     * Permet de valider le checksum d'un message passé en paramètre qui commence par $ et qui fini par \n ou non
     * @param msg_nmea , message à recu
     * @param type, variable de retour avec le type de message (ex: GGA) 
     * @param datas, variable de retour avec les données du message (ex:5300.97914,N,00259.98174,E,125926,A)
     * @return si < 0 alors, il s'agit d'une erreur
     */
    static int parseNmeaMsg(string msg_nmea , NMEA_trameType * type , NMEA_datas *datas){
        while(msg_nmea.back() == '\n' || msg_nmea.back() == '\r') msg_nmea.pop_back();
        
        // on check le checksum
        int ret = 0;
        if((ret = validateChecksum(msg_nmea)) < 0){
            return ret;
        }
        
        string sentence = msg_nmea.substr(1); // on supprime le "$"
        
        std::size_t found =sentence.find('*'); // on cherche la position de l'étoile
        if (found==std::string::npos) return -3;
        
        sentence = sentence.erase(found, sentence.size()-found); // on supprime le checksum
        
        //cout << "msg:" << sentence << endl;
        
        // split de la trame
        std::string delimiter = ",";
        size_t pos = 0;
        *type = "undef";
        // si pas de virgule
        if(sentence.find(delimiter) == std::string::npos){
            *type = sentence;
        }else{
            while ((pos = sentence.find(delimiter)) != std::string::npos) {
                if(type->compare("undef") == 0)*type = sentence.substr(0, pos);
                else datas->push_back(sentence.substr(0, pos));
                sentence.erase(0, pos + delimiter.length());
            }
            datas->push_back(sentence.substr(0, pos));
        }
        //for(int i = 0;i < datas->size();i++)cout << "datas:"<<datas->at(i) << endl; 
        return 0;
                       
    }
    
    /**
     * Permet de valider le checksum d'un message passé en paramètre qui commence par $ et qui fini par \n ou non
     * @param msg , message à valider
     * @return si < 0 alors, il s'agit d'une erreur
     */
    static int validateChecksum(NMEA_string msg){
        while(msg.back() == '\n' || msg.back() == '\r') msg.pop_back();
        string sentence = msg.substr(1); // on supprime le "$"
        //sentence = string(sentence,sentence.size()-1);
        
        std::size_t found =sentence.find('*'); // on cherche la position de l'étoile
        if (found==std::string::npos) return -1;
        
        string checksum = (sentence.substr(found+1)); // récupération du checksum
        std::transform(checksum.begin(), checksum.end(), checksum.begin(),
            [](unsigned char c){ return std::tolower(c); });
        
        sentence = sentence.erase(found, sentence.size()-found); // on supprime le checksum
        
        
        //cout << "msg:" << sentence << ":" << checksum << "==" <<calculateChecksum(sentence) <<endl;
        
        //on recalcule le checksum et on le compare à celui réceptionné
        string checksum_cal = calculateChecksum(sentence);
        if(checksum.compare(checksum_cal) != 0){
            return -2;
        }
        
        return 0;
    }
    
    /**
     * Permet de calculer le checksum d'un message (sans le $) passé en paramètre
     * @param msg , message
     * @return si < 0 alors, il s'agit d'une erreur
     */
    static std::string calculateChecksum(NMEA_string msg){

        unsigned int checksum = 0;
        for (int i = 0; i < msg.length(); i++)
            checksum ^= (unsigned int) msg.at(i);
        
        std::stringstream result;
        if(checksum < 0x10)result << "0";
        result << std::hex << checksum; // convertion en hexa
        
        return result.str();
    }

private:
    NMEA_string _msg;
    
protected:

};

#endif /* NMEA_H */
