/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReadParameters.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 26 octobre 2016, 10:48
 */

#ifndef READPARAMETERS_H
#define READPARAMETERS_H

#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>


using namespace std;

/**
 * Liste de paramètres
 */
class Parameters : public map<string,string>{
public:
    Parameters(){};
    virtual ~Parameters(){};
    string toString(){
        std::ostringstream ret;
        for (Parameters::iterator it=begin(); it!=end(); ++it)
            ret << it->first << " = " << it->second << '\n';
        return ret.str();
    };
    double getDoubleParameter(string nameParameter, bool& isPresent){
        string value = getStringParameter(nameParameter,isPresent);
        if(isPresent){
            return stof(value);
        }
        return 0.0;
    }
    int getIntParameter(string nameParameter,bool& isPresent){
        string value = getStringParameter(nameParameter,isPresent);
        if(isPresent){
            return stoi(value);
        }
        return 0;
    }
    string getStringParameter(string nameParameter,bool& isPresent){
        for (Parameters::iterator it=begin(); it!=end(); ++it)
            if(it->first == nameParameter){
                isPresent = true;
                return it->second;
            }
        isPresent = false;
        return "undef";
    }
    int getStructParameter(string nameParameter,string* parameters , int numberParameter  ,bool& isPresent){
        for (Parameters::iterator it=begin(); it!=end(); ++it)
            if(it->first == nameParameter){

                string datas = it->second;
                // on supprime les crochets
                datas.erase(0,1);
                datas.erase(datas.size()-1,1);
                
                int inc = 0;
                while(1){
                    // trop de paramètre trouvé
                    if(inc >= numberParameter){
                        return -1;
                    }else{
                        parameters[inc] = datas.substr(0, datas.find(","));
                        
                        if(datas.find(",") == string::npos){
                            isPresent = true;
                            return inc+1;
                        }
                    }
                    inc++;
                    datas.erase(0, datas.find(",") + 1);
                }
                
            }
        isPresent = false;
        return -2;
    }
};


/**
 * Classe permettant de gérer la lecture d'un fichier et de récupérer une liste de paramètre
 */
class ReadParameters {
public:
    /**
     * Constructeur
     * @param configFile, fichier de config à parser
     */
    ReadParameters(string configFile);
    /**
     * destructeur
     */
    virtual ~ReadParameters();
    /**
     * retourne la liste de paramètre contenu dans le fichier sous la forme "key=value"
     * @return La liste des paramètres
     */
    Parameters getParameters();
    
private:
    Parameters _params;
    string _confPathFile;
    ifstream _file;

};

#endif /* READPARAMETERS_H */

