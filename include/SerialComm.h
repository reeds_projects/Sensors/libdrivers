/* 
 * File:   SerialComm.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Librairie basée sur CSerial de Luc Rossi / Syera
 * 
 * Created on 9 septembre 2016, 16:18
 */

#ifndef SERIALCOMM_H
#define SERIALCOMM_H

#include <string>
#include <ctime>
#include <termios.h>

#include <sys/time.h>
#include <time.h>

#include "Communication.h"

// valeurs possible pour les codes de retour
#define SERIAL_OK		1
#define SERIAL_ERR		-1
#define SERIAL_ERR_TIMOUT	-2
#define SERIAL_ERR_FRAME	-3
#define SIZE_NAME_PORT		80	

class SerialComm : public Communication{
public:
    /**
     * Constructeur
     */
    SerialComm();
    
    /**
     * Destructeur
     */
    virtual ~SerialComm();
    
    /**
     * Permet de se connecter à un port série
     * @param path,le port et la vitesse (exemple : /dev/ttyUSB0:115200)
     * @return -1 si erreur
     */
    int connection(string path);
    
    /**
     * Permet de lire les données sur le lien de communication avec un caractère de début de trame
     * @param charStart, caractère de début de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,char *buff,int length);
    
    /**
     * Permet de lire les données sur la liaison série
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff,int length);
    
    /**
     * Permet de lire les données sur la liaison série
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff, int length, int timeout_ms);
    
    /**
     * Permet de lire les données sur la liaison série
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout_ms);
    
    /**
     * Permet de lire les données sur la liaison série
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff);
    
    /**
     * Permet d'écire des données sur la liaison série
     * @param data, chaine de caractère à envoyer sur la socket
     * @return 0 si ok, -1 si erreur
     */
    int writeDatas(const string &data);
    
    /**
     * Permet d'envoyer une structure sur la liaison série
     * @param datas, la structure
     * @param sizeDatas, la taille de la commande
     * @return 0 si ok
     */
    int writeDatas( void *datas, int sizeDatas );
    
     /**
     * Permet de faire la déconnexion 
     */
    void disconnect();
    
private:
    /**
     * Permet de vider le buffer
     */
    void flush_buffer(void) ;

};

#endif /* SERIALCOMM_H */

