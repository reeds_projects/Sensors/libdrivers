/* 
 * File:   Tcp2serial.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 8 septembre 2016, 12:19
 */

#ifndef TCP2SERIAL_H
#define TCP2SERIAL_H

#include <string>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>

#include "Communication.h"

using namespace std;
/**
 * Permet de créer une communication TCP 
 */
class TcpComm : public Communication {
public:
    /**
     * Constructeur
     */
    TcpComm();
    
    /**
     * Destructeur
     */
    virtual ~TcpComm();
    
    /**
     * Permet de se connecter à la socket
     * @param server,  adresse IP du serveur TCP et le port (exemple : 192.168.1.100:1470)
     * @return -1 si erreur
     */
    int connection(string server);

    /**
     * Permet de lire les données sur la socket TCP
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff,int length);
    
    /**
     * Permet de lire les données sur la socket TCP
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff,int length, int timeout_ms);
    
    /**
     * Permet de lire les données sur la socket TCP
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout_ms);
    
    /**
     * Permet de lire les données sur la socket TCP
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff);

    /**
     * Permet de lire les données sur le lien de communication avec un caractère de début de trame
     * @param charStart, caractère de début de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,char *buff,int length);

    
    /**
     * Permet d'écire des données sur la socket tcp
     * @param data, chaine de caractère à envoyer sur la socket
     * @return 0 si ok, -1 si erreur
     */
    int writeDatas(const string &data);
    
    /**
     * Permet d'envoyer une structure sur la socket TCP
     * @param datas, la structure
     * @param sizeDatas, la taille de la commande
     * @return 0 si ok
     */
    int writeDatas( void *datas, int sizeDatas );
    
    /**
     * Permet de faire la déconnexion 
     */
    void disconnect();

    /**
     * Callback appeler lors de la perte de connexion (CONNECTION_RESET_BY_PEER est retourné par waitDatas())
     * @return si < 0 alors, il s'agit d'une erreur
     */
    int connectionLost();

    
private:
    //server
    struct sockaddr_in _target;
    char _buff[2048];
    int _buff_position;
    string _lastConnexionInfo;


};

#endif /* TCP2SERIAL_H */

