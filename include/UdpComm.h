/* 
 * File:   UdpComm.h
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 *
 * Created on 9 septembre 2016, 14:37
 */

#ifndef UDPCOMM_H
#define UDPCOMM_H

#include <string>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <stdlib.h>

#include "Communication.h"


using namespace std;

/**
 * Permet de créer une communication UDP
 */
class UdpComm : public Communication {
public:
    /**
     * Constructeur
     */
    UdpComm();
    
    /**
     * Destructeur
     */
    virtual ~UdpComm();
    
    /**
     * Permet de se connecter à la socket
     * @param server,  adresse IP du serveur UDP et le port (exemple : 192.168.1.100:1470),
     * Ajouter -S après le port pour créer un serveur (exemple:127.0.0.1:9077:-S)
     * @return -1 si erreur
     */
    int connection(string server);
    
    /**
     * Permet de lire les données sur la socket UDP
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff, int length);

    /**
     * Permet de lire les données sur la socket UDP
     * @param buff, buffer de caractères contenant les données à lire
     * @param length, nombre de caractère à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(char *buff,int length, int timeout_ms);
    
    /**
     * Permet de lire les données sur la socket UDP
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout_ms);
    
    /**
     * Permet de lire les données sur la socket UDP
     * @param charStart, caractère de début de trame
     * @param charEnd, caractère de fin de trame
     * @param buff, buffer de caractères contenant les données à lire
     * @param timeout, temps en ms du timeout
     * @return si <= 0 , il s'agit d'erreur
     */
    int readDatas(unsigned char charStart,unsigned char charEnd,char *buff);
    
    /**
     * Permet d'écire des données sur la socket UDP
     * @param data, chaine de caractère à envoyer sur la socket
     * @return 0 si ok, -1 si erreur
     */
    int writeDatas(const string &data);
    
    /**
     * Permet d'envoyer une structure sur la socket UDP
     * @param datas, la structure
     * @param sizeDatas, la taille de la commande
     * @return 0 si ok
     */
    int writeDatas( void *datas, int sizeDatas );
    
    /**
     * Permet de faire la déconnexion 
     */
    void disconnect();
    
private:
    //server
    struct sockaddr_in _target;
    
};

#endif /* UDPCOMM_H */

