/* 
 * File:   baseDriver.cpp
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 * 
 * Created on 8 septembre 2016, 11:50
 */

#include "BaseDriver.h"
#include "SerialComm.h"
#include "UdpComm.h"
#include "TcpComm.h"
#include "FileComm.h"
#include "HILComm.h"

BaseDriver::BaseDriver(string logPathFile) 
    : _comm(NULL)
    , _logPathFile(logPathFile)
    , _timeLastReception(0.0)
    , _lastConnectionInfo("")
    ,_file(logPathFile, ".csv", LOG_FILE_SIZE)// ouverture du fichier de log
    , _numberLineHeader(0)
{           
    std::locale::global(std::locale::classic());
}

BaseDriver::~BaseDriver() {
    if(_comm != NULL){
        disconnect();
        delete _comm;
    }
}

bool BaseDriver::isConnected(){
    return _comm->isConnected();
}

void BaseDriver::setDriverName(string driverName){
    _driverName = driverName;
}

string BaseDriver::getDriverName(){
    return _driverName;
}

void BaseDriver::setDriverDescription(string description){
    _driverDescription = description;
}

string BaseDriver::getDriverDescription(){
    return _driverDescription;
}

void BaseDriver::setDriverVersion(int major, int minor){
    _driverVersion = to_string(major)+"."+to_string(minor);
}

string BaseDriver::getDriverVersion(){
    return _driverVersion;
}

int BaseDriver::getNumberOfLineForHeader(){
    return _numberLineHeader;
}


int BaseDriver::initializeWithParameters(Parameters params){
    
    bool isPresent = false;
    //récupération du type de communication 
    bool b_enableLog = params.getIntParameter("ENABLE_LOG",isPresent);
    if(!isPresent){
        printf("WARNING key 'ENABLE_LOG' not found \n");
    }else{
        _logActived = b_enableLog;
    }
    return 0;
}

int BaseDriver::connectUDP(const char * sensorDevPath) {
    int ret = connect(BaseDriver::UDP,sensorDevPath);
#ifdef __DEBUG_FLAG_
    cout << "connectUDP :" << ret << std::endl;
#endif
    // si la connexion est ok
    if(ret >= 0 )return initialize();
    else return ret;
}

int BaseDriver::connectTCP(const char * sensorDevPath) {
    int ret = connect(BaseDriver::TCP,sensorDevPath);
#ifdef __DEBUG_FLAG_
    cout << "connectTCP :" << ret << std::endl;
#endif
    // si la connexion est ok
    if(ret >= 0 )return initialize();
    else return ret;
}

int BaseDriver::connectSerial(const char * sensorDevPath){
    int ret = connect(BaseDriver::SERIAL,sensorDevPath);
#ifdef __DEBUG_FLAG_
   cout << "connectSerial :" << ret << std::endl;
#endif
   
    // si la connexion est ok
    if(ret >= 0 )return initialize();
    else return ret;
    
}

int BaseDriver::connectFile(const char * file){
    int ret = BaseDriver::connect(BaseDriver::FILE,file);
#ifdef __DEBUG_FLAG_
    cout << "Connection FILE :" << ret << std::endl;
#endif
    
    // si la connexion est ok
    if(ret >= 0 )return initialize();
    else return ret;
}

int BaseDriver::connectHIL(const char * hilDevPath){
    int ret = BaseDriver::connect(BaseDriver::HIL,hilDevPath);
#ifdef __DEBUG_FLAG_
    cout << "Connection HIL :" << ret << std::endl;
#endif

    // si la connexion est ok
    if(ret >= 0 )return initialize();
    else return ret;
}

int BaseDriver::connectWithConfigFile(string configFile){
    int ret = connect(configFile);
#ifdef __DEBUG_FLAG_
    cout << "Connection with config file ("<< configFile <<"):" << ret << std::endl;
#endif
    // si la connexion est ok
    if(ret >= 0 )return initialize();
    else return ret;
}

int BaseDriver::connect(string configFile){
        
    // récupération des paramètres 
    ReadParameters readParams(configFile);
    Parameters params = readParams.getParameters();
    
    bool isPresent = false;
    //récupération du type de communication 
    string s_typeComm = params.getStringParameter("TYPE_COMM",isPresent);
    if(!isPresent){
        printf("Error key 'TYPE_COMM' not found \n");
        return -1;
    }
    
    Type_comm typeComm;
    if(s_typeComm == "SERIAL") typeComm = SERIAL;
    else if(s_typeComm == "TCP") typeComm = TCP;
    else if(s_typeComm == "UDP") typeComm = UDP;
    else if(s_typeComm == "FILE") typeComm = FILE;
    else if(s_typeComm == "HIL") typeComm = HIL;
    else{
        printf("Error of value : values valid for 'TYPE_COMM' are SERIAL, UDP, TCP, FILE \n");
        return -2;
    }
    
    //récupération de l'information de connexion
    string s_infoConn = params.getStringParameter("INFO_CONN",isPresent);
    if(!isPresent){
        printf("Error key 'INFO_CONN' not found \n");
        return -1;
    }

    //on lance la connexion
    int ret = 0;
    if( (ret = connect(typeComm,s_infoConn)) < 0)return ret;
    
//    printf("ret:%d : %s\n",ret,s_infoConn.c_str());
//    sleep(2);
        
    // on lance l'initialisation avec les parametres du fichier de configuration
    return initializeWithParameters(params);
    
}

int BaseDriver::connect(Type_comm typeComm,string infoConnection){
    // on stock le type de comm
    _commType = typeComm;
    
    // s'il y a déjà une connexion, on déconne et on réalise la nouvelle
    if(_comm != NULL){
        disconnect();
        delete _comm;
    }

    // permet de sélectionner le type de connexion
    if(typeComm == SERIAL){
        _comm = new SerialComm();
    }else if(typeComm == UDP){
        _comm = new UdpComm();
    }else if(typeComm == TCP){
        _comm = new TcpComm();
    }else if(typeComm == FILE){
        _comm = new FileComm();
    }else if(typeComm == HIL){
        _comm = new HILComm();
    }

    // init time de référence
    _startApp = chrono::system_clock::now();
    
    //lancement de la connexion
    _lastConnectionInfo = infoConnection;
    int ret = _comm->connection(infoConnection);
    //if(ret >= 0)_isConnected = true;
    return ret;

    
}

int BaseDriver::reconnect(){
    if(_lastConnectionInfo == ""){
        cout << "BaseDriver::reconnect() : last connection info is empty "<<std::endl;
        return -1;
    }
    int ret = connect(_commType,_lastConnectionInfo);
    if( ret >= 0)reconnected();
    return ret;
}

void BaseDriver::disconnect(){
    _comm->disconnect();
}

void BaseDriver::printToLogFile(string data){
    unsigned long milliseconds_since_epoch =
        std::chrono::system_clock::now().time_since_epoch() /
        std::chrono::milliseconds(1);
    if(_logActived){
        _file << std::to_string(milliseconds_since_epoch)<<";";
        _file << data.c_str();
        _file << flush;
    }
}

int BaseDriver::writeDatas(unsigned char *buff,int length){
    if(_comm == NULL) return -1;
    return _comm->writeDatas((char *)buff,length);
}

int BaseDriver::writeDatas(string data){
    if(_comm == NULL) return -1;
    return _comm->writeDatas(data);
}

int BaseDriver::writeDatas(NMEA_msg msg){
    if(_comm == NULL) return -1;
    return _comm->writeDatas(msg.toString());
}

int BaseDriver::writeDatas( void *datas, int sizeDatas ){
    if(_comm == NULL) return -1;
    return _comm->writeDatas(datas,sizeDatas);
}


int BaseDriver::readDatas(unsigned char charStart,char *buff,int length){
    if(_comm == NULL) return -1;
    int ret = _comm->readDatas(charStart,buff,length);
    
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}

int BaseDriver::readDatas(char *buff,int length){
    if(_comm == NULL) return -1;
    int ret = _comm->readDatas(buff,length);
    
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}

int BaseDriver::readDatas(char *buff,int length, int timeout){
    if(_comm == NULL) return -1;
    int ret = _comm->readDatas(buff,length,timeout);
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
    
}

int BaseDriver::readDatas(unsigned char charStart,unsigned char charEnd,char *buff){
    if(_comm == NULL) return -1;
    int ret = _comm->readDatas(charStart,charEnd,buff);
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}

int BaseDriver::readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout){
    if(_comm == NULL) return -1;
    int ret = _comm->readDatas(charStart,charEnd,buff,timeout);
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}

string BaseDriver::readDatasToString(int length,int *ok ){
    if(_comm == NULL) return "Not connected";
    string ret = _comm->readDatasToString(length,ok);
    
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}

string BaseDriver::readDatasToString(char charactere,int *ok ){
    if(_comm == NULL) return "Not connected";
    string ret = _comm->readDatasToString(charactere,ok);
    
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}

string BaseDriver::readDatasToString(int length, int timeout,int *ok ){
    if(_comm == NULL) return "Not connected";
    string ret = _comm->readDatasToString(length,timeout,ok);
    
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}


string BaseDriver::readDatasToString(char charactere, int timeout,int *ok ){

    if(_comm == NULL) return "Not connected";
    string ret = _comm->readDatasToString(charactere,timeout,ok);

#ifdef _DEBUG_TIMING
   	_endReceived = std::chrono::high_resolution_clock::now();
#endif
    
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}

string BaseDriver::readDatasToString(char charactereStart,char charactereEnd, int timeout,int *ok){
    if(_comm == NULL) return "Not connected";
    string ret = _comm->readDatasToString(charactereStart,charactereEnd,timeout,ok);
#ifdef _DEBUG_TIMING
   	_endReceived = std::chrono::high_resolution_clock::now();
#endif
    
    // calcul du timestamp en seconde
    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = now-_startApp;
    _timeLastReception = elapsed_seconds.count();
    
    return ret;
}


void BaseDriver::enableLogging(bool enable){
    _logActived = enable;
}

bool BaseDriver::logActived(){
    return _logActived;
}

double BaseDriver::getLastReceptionTime(){
    return _timeLastReception;
}

BaseDriver::Type_comm BaseDriver::getCommunicationType(){
    return _commType;
}

int BaseDriver::read(){
#ifndef MAIN_APP

        string datas = waitDatas();
        //printf("read:%s\n",datas.c_str());
        if(datas.compare(COMMUNICATION_TIMEOUT) == 0){
            printf("Connexion timeout\n");
            timeout();
            return -1;
        }else if(datas.compare(CONNECTION_RESET_BY_PEER) == 0){
            printf("Connexion \"reset by peer\"\n");
            connectionLost(_comm->connectionLost());
            return  -2;
        }else if(datas.compare(CONNECTION_NOT_CONNECTED) == 0){
            printf("Not connected\n");
            reconnect();
            return  -3;
        }else{
            if(_commType == FILE){
                return readOnFile(datas);
            }else if(_commType == HIL){
                return readOnHILServer(datas);
            }else{
                return readOnSensor(datas);
            }
        }
#endif
}

void BaseDriver::timeout(){}

void BaseDriver::connectionLost( int reconn_result){
    if(reconn_result < 0){
        printf("Error to Reconnect\n");
    }else{
        printf("Reconnected\n");
        reconnected();

       if(initialize() < 0){
           printf("Error to initialize\n");
       }
    }


}

int BaseDriver::write(unsigned char * cmd , int size){

#ifdef __DEBUG_FLAG_
    cout << "write:" << cmd << "size: " << size <<std::endl;
#endif

    int ret = 0;
    if ( (ret = writeDatas(cmd, size)) < 0) {
        printf("Error writing data\n");
        return -1;
    }
    
    return ret;
    
}

int BaseDriver::write( void *datas, int sizeDatas ){
#ifdef __DEBUG_FLAG_
    cout << "write:" << datas << "size: " << sizeDatas <<std::endl;
#endif
    
    int ret = 0;
    if ( (ret = writeDatas(datas, sizeDatas)) < 0) {
        printf("Error writing data\n");
        return -1;
    }
    
    return ret;
}

void BaseDriver::setSensorOrientation(double phi, double theta, double psi){
    _frameshiftLocal.setPhiDeg(phi);
    _frameshiftLocal.setThetaDeg(theta);
    _frameshiftLocal.setPsiDeg(psi); 
}

Frameshift BaseDriver::getSensorOrientation(){
    return _frameshiftLocal;
}

bool BaseDriver::isTCPCommunication(){
    return getCommunicationType() == TCP ? true:false;
}

bool BaseDriver::isUDPCommunication(){
    return getCommunicationType() == UDP ? true:false;
}

bool BaseDriver::isSERIALCommunication(){
    return getCommunicationType() == SERIAL ? true:false;
}

bool BaseDriver::isFILECommunication(){
    return getCommunicationType() == FILE ? true:false;
}

Communication * BaseDriver::getCommunication(){
    return _comm;
}

void BaseDriver::setCommunication(Communication * comm){
    _comm = comm;
}

long int BaseDriver::getTimeSystem(){
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

int BaseDriver::initialize(){
    string header = "";
    header += getDriverName()+" "+getDriverVersion()+"\n";
    header += getDriverDescription()+"\n";
    header += "INFO separateur=;";
    header += "DATAFMT 02.00\n";
    header += "DATANAMES\n";
    header += "timesinceepoch "+logHeadersVarNames()+"\n";
    header += "UNITS\n";
    header += "ms "+logHeadersVarUnits()+"\n";
    header += "DATA\n";

    _numberLineHeader = std::count(header.begin(), header.end(), '\n');

    _file << header.c_str();
    _file << flush;
    return 0;
}

Vector3D BaseDriver::frameshiftLocal(double x, double y , double z){
    return _frameshiftLocal.shifter(x,y,z);
}

NormalizedAngle BaseDriver::normalizeAngleLocal(double phi, double theta , double psi){
    return _frameshiftLocal.normalizeAngle(phi,theta,psi);
}
