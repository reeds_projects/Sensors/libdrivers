/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileComm.cpp
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 * 
 * Created on 2 novembre 2016, 15:08
 */

#include "FileComm.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <vector>
#include <sstream>
#include <sys/types.h>
#include <thread>

FileComm::FileComm() 
    : _period_us(100000)//100ms
    , _currentIndexLine(0)
{
}

FileComm::~FileComm() {
}

int FileComm::connection(string path) {
    
    // permet de recuperer le port et la vitesse
    vector<string> elems;
    stringstream ss(path.c_str());
    string item;
    while (getline(ss, item, ':')) {
        elems.push_back(item);
    }
    // on verifie que l'on a bien 2 éléments(fichier + frequence)
    if(elems.size() != 2){
        perror("Error in the format of Path (format: /directory/file.txt:frequence)");
        return -1;
    }
    
    //résupération de l'adresse et de la vitesse de lecture des trames
    const char* path_file = elems.at(0).c_str();
    _period_us = (1000000 / stoi(elems.at(1).c_str()));
    
    // on verifie que le fichier est présent
    if(access(path_file, F_OK)){
        printf("Error: replay file not found\n");
        return -2;
    }

    //chargement des index de ligne
    
    int fd = open( path_file, O_RDONLY) ;

    if(fd<0) { 
        printf("connection FILE: open err=%d (%s)\n",errno,strerror(errno)) ; 
        return(fd) ; 
    }
    _fileDescriptor=fd ;	// memorise descripteur du port

    _lineTable.clear();
    _lineTable.push_back(0);

    FILE *fp;

    fp = fopen(path_file,"r");
    if(fp == NULL) {
        printf("Error to load line table") ;
        return(-3) ;
    }

    char ch;
    unsigned long count = 0;
    string timestamp = "";
    bool timestampFound = false;
    while ((ch = getc(fp)) != EOF) {
        count++;
        if(ch == '\n'){
            timestampFound = false; // reset du flag pour recommencer la recherche du timestamp
            _lineTable.push_back(count);
        //recherche du timestamp
        }else if(timestampFound == false){
            if(ch == ';'){
                timestampFound = true;//on arrete la recherche du timestamp
                //printf("%s %lu\n",timestamp.c_str(),stol(timestamp));
                _lineTimestampTable.push_back(stol(timestamp));
                timestamp = "";
            }else{
                timestamp += ch;
            }

        }
    }
    fclose(fp);

//    printf("number line %d == %d \n",_lineTable.size(),_lineTimestampTable.size());

//    for(int i = 0 ; i < _lineTable.size();i++){

//         printf("%d\n",_lineTable.at(i));
//    }

//    for(int i = 0 ; i < _lineTimestampTable.size();i++){

//         printf("line[%d] : timestamp=%lu\n",i,_lineTimestampTable.at(i));
//    }

//    lseek(_fileDescriptor,_lineTable.at(9),SEEK_SET);

//    //on saute les ligne d'entete
//    for(int i = 0 ; i < 9 ; i++){
//      readDatasToString('\n');
//    }

     _isConnected = true;
    
    return(_fileDescriptor) ;

}

void FileComm::disconnect(){
    // ferme le port
    close(_fileDescriptor) ;
    _fileDescriptor=-1 ;
     _isConnected = false;
}

int FileComm::writeDatas(const string &data ){
    return(data.size()) ;
}

int FileComm::writeDatas( void *datas, int sizeDatas ){
    return(sizeDatas);
}

int FileComm::readDatas(char *buff,int length){
    
    int nb_read=0 ;

    if(_fileDescriptor<0) return(FILE_ERR) ;    // controle existence port
    
    do{
        if((nb_read = read(_fileDescriptor , buff , length)) > 0 ){
            //printf("read %d car\n",nb_read) ;
        }
    }while(nb_read < length);
    
    return (nb_read) ;
}

int FileComm::readDatas(char *buff,int length, int timeout_ms){

    int nb_read=0 ;
    bool sortie_timeout=false ;

    if(_fileDescriptor<0) return(FILE_ERR) ;    // controle existence port

    //printf("debut ReceiveSerial ") ;

    // arme timeout
    _timeout = std::chrono::system_clock::now();


    // reception de la trame
    do{
        if(read(_fileDescriptor , buff+nb_read , 1) >0 ){
            //printf("{%02X}\n",*(buff+nb_read)) ;
            nb_read++ ;
        }

        if( nb_read==length)
        {
            break ;
        }

        usleep(20) ;   //  modif 10/04/2013 pour eviter attente active

        if( std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - _timeout).count() >= timeout_ms ){
            sortie_timeout=true ;
            break ;
        }

    } while( 1 ) ;
    

    // sortie en timeout ?
    if( sortie_timeout ){
        //printf("TIMEOUT lu=%d taille=%d (handle:%d)\n",nb_read,taille,port_handle) ;
        // retour erreur timeout seulement si aucun car recu
        return(nb_read==0?FILE_ERR_TIMOUT:nb_read);
    }
    //printf("trame recue !") ;

    return(nb_read) ;
}

int FileComm::readDatas(unsigned char charStart, unsigned char charEnd, char *buff, int timeout_ms){
    int nb_read=0 ;
    bool sortie_timeout=false ;
    int isStarted = 0;
    
    if(_fileDescriptor<0) return(FILE_ERR) ;    // controle existence port
    
    //printf("debut ReceiveSerial ") ;
    
    // arme timeout
    _timeout = std::chrono::system_clock::now();
    
    // reception de la trame
    do{
        if(read(_fileDescriptor , buff+nb_read , 1) >0 ){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
        }
        
        if( *(buff+(nb_read-1)) == charEnd){
            *(buff+(nb_read)) = '\0';
            break ;
        }
        
        usleep(20) ;   //  modif 10/04/2013 pour eviter attente active
        
        if( std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - _timeout).count() >= timeout_ms ){
            sortie_timeout=true ;
            break ;
        }
        
    } while( 1 ) ;
    
    
    // sortie en timeout ?
    if( sortie_timeout ){
        //printf("TIMEOUT lu=%d taille=%d (handle:%d)\n",nb_read,taille,port_handle) ;
        // retour erreur timeout seulement si aucun car recu
        return(nb_read==0?FILE_ERR_TIMOUT:nb_read);
    }
    //printf("trame recue !") ;
    
    return(nb_read) ;
}

int FileComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff){
    int nb_read=0 ;
    int isStarted = 0;
    
    if(_fileDescriptor<0) return(FILE_ERR) ;    // controle existence port
    
    //printf("debut ReceiveSerial ") ;
    
    
    // reception de la trame
    do{
        if(read(_fileDescriptor , buff+nb_read , 1) >0 ){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
        }
        
        if( *(buff+(nb_read-1)) == charEnd){
            *(buff+(nb_read)) = '\0';
            break ;
        }
        
        usleep(20) ;   //  modif 10/04/2013 pour eviter attente active
        
    } while( 1 ) ;
    
    //printf("trame recue !") ;
    
    return(nb_read) ;
}


string FileComm::readDatasToString(int length,int *ok ){
    unsigned long wait_ms = getNextLineTimestamp() - getCurrentLineTimestamp();
    std::this_thread::sleep_for (std::chrono::milliseconds(wait_ms));
    string ret = Communication::readDatasToString(length,ok);
    if(ret != "") _currentIndexLine++;
    return ret;
}
    
string FileComm::readDatasToString(char charactere,int *ok){
    unsigned long wait_ms = getNextLineTimestamp() - getCurrentLineTimestamp();
    //printf("FileComm :: readDatasToString(char charactere,int *ok) : wait:  (%lu - %lu) = %lu",getNextLineTimestamp(),getCurrentLineTimestamp(),wait_ms) ;
    std::this_thread::sleep_for (std::chrono::milliseconds(wait_ms));

    string ret = Communication::readDatasToString(charactere,ok);
    if(ret != "") _currentIndexLine++;
    return ret;
}

string FileComm::getLineAt(unsigned int lineNumber, int *ok){
    if(lineNumber > _lineTable.size()){
        printf("FileComm :: getLineAt : line number out of range ") ;
        if(ok != nullptr) *ok = -1;
        return "";
    }

    //on stock le timestamp courant
    _currentIndexLine = lineNumber;
    lseek(_fileDescriptor,_lineTable.at(lineNumber),SEEK_SET);

    return readDatasToString('\n',ok);
}

string FileComm::nextLine(int *ok){
    if(_currentIndexLine+1 > _lineTable.size()-1 ){
        _currentIndexLine = 0;
    }
    _currentIndexLine++;
    return getLineAt(_currentIndexLine,ok);

}

string FileComm::previousLine(int *ok){

    if(_currentIndexLine-1 < 1 ){
        _currentIndexLine = _lineTable.size()-1;
    }
    _currentIndexLine--;
    return getLineAt(_currentIndexLine,ok);
}

string FileComm::currentLine(int *ok){
    return getLineAt(_currentIndexLine,ok);
}

unsigned int FileComm::currentLineNumber(){
    return _currentIndexLine;
}

unsigned int FileComm::getNumberOfLine(){
    return _lineTable.size()-1;
}

unsigned long FileComm::getCurrentLineTimestamp(){
    return _lineTimestampTable.at(_currentIndexLine);
}

unsigned long FileComm::getNextLineTimestamp(){
    if(_currentIndexLine+1 > _lineTimestampTable.size()) return _lineTimestampTable.back();
    return _lineTimestampTable.at(_currentIndexLine+1);
}

unsigned long FileComm::getPreviousLineTimestamp(){
    if(_currentIndexLine-1 < 0) return _lineTimestampTable.front();
    return _lineTimestampTable.at(_currentIndexLine-1);
}
