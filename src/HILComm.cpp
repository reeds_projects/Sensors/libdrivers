
#include "HILComm.h"
#include <vector>
#include <sstream>

HILComm::HILComm()
{
    //initialisation de la connection TCP
    memset((void *)&_target, 0, sizeof(_target));

    _buff_position = 0;

}

HILComm::~HILComm() {

}

int HILComm::connection(string server) {

    if(_fileDescriptor == -1){
        //creation de la socket
        _fileDescriptor = socket(AF_INET , SOCK_STREAM , 0);
        if (_fileDescriptor == -1){
            perror("Could not create socket");
        }
    }

    // permet de recuperer l'adresse IP et le port du serveur
    vector<string> elems;
    stringstream ss(server.c_str());
    string item;
    while (getline(ss, item, ':')) {
        elems.push_back(item);
    }
    // on verifie que l'on a bien 2 éléments(adresse + port)
    if(elems.size() != 2){
        perror("Error in the format of IP address");
        return -1;
    }

    //résupération de l'adresse et du port
    string server_ip = elems.at(0);
    int port = stoi(elems.at(1).c_str());

    //setup address structure
    if(inet_addr(server_ip.c_str()) == -1){
        struct hostent *host;
        struct in_addr **addr_list;

        //résolution du nom , si ce n'est pas une adresse IP
        if ( (host = gethostbyname( server_ip.c_str() ) ) == NULL){
            herror("gethostbyname\n");
            return false;
        }

        //recupération de la liste d'adresse
        addr_list = (struct in_addr **) host->h_addr_list;
        for(int i = 0; addr_list[i] != NULL; i++){
            _target.sin_addr = *addr_list[i];
            break;
        }
    }else{
        _target.sin_addr.s_addr = inet_addr( server_ip.c_str() );
    }

    _target.sin_family = AF_INET;
    _target.sin_port = htons( port );

    //Connexion
    int ret = 0;
    if ((ret = connect(_fileDescriptor, (struct sockaddr *)&_target , sizeof(_target))) < 0){
        close(_fileDescriptor);
        perror("connect failed. Error\n");
    }

     _isConnected = true;

    return ret;

}

int HILComm::readDatas(char *buff,int length){
    //réception depuis le serveur
    int nb_read = 0;
    if( (nb_read = recv(_fileDescriptor , buff , length , 0)) < 0){
        puts("recv failed");
    }
    return nb_read;
}

int HILComm::readDatas(char *buff,int length, int timeout_ms){

    int nb_read = 0;
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = timeout_ms*1000;// passage de ms -> us
    // ajout du timeout
    setsockopt(_fileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

    //lecture de la trame
    if ((nb_read = recv(_fileDescriptor, buff, length, 0)) == -1){
        // si erreur
        if ((errno != EAGAIN) && (errno != EWOULDBLOCK)){
            perror("Error readDatas ");
        }
    }

    return nb_read;
}

int HILComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout_ms){

    //printf("------------------readDatas \n");

//    do{
//        int ret = 0;
//        //lecture de 256 caractère
//        if((ret = recv(_fileDescriptor, _buff+_buff_position, 256, 0))>0){
//            //modification de la position du pointeur sur le buffer
//            _buff_position += ret;
//            //printf("lu %d  -> position index :%d\n",ret, _buff_position);
//
//            //position du pointeur de début de trame
//            int pos_start = 0;
//            int pos_end = 0;
//            //on parcourt la trame de réponse
//            for(int i = 0 ; i < _buff_position ; i++){
//
//                //printf("{%02X} = %c \n",*(_buff+i),*(_buff+i)) ;
//                //printf("{%02X} ",*(_buff+i)) ;
//                //recherche du début de la trame
//                if( *(_buff+i) == charStart ) {
//                    pos_start = i;
//                    //printf("debut Trame \n");
//                //recherche de la fin de la trame
//                }else if(*(_buff+i) == charEnd){
//
//                    //printf("fin Trame \n");
//                    pos_end = i;
//                    strncpy(buff , _buff+pos_start , pos_end-pos_start + 1);
//                    buff[pos_end-pos_start + 1] = '\0';
//
////                    for(int j = 0 ; j < pos_end-pos_start + 1 ; j++){
////                        printf("{%02X} ",*(buff+j)) ;
////                    }
////                     printf("\n apres \n") ;
//
//                    // décalage du buffer
//                    int indexBuff = 0;
//                    for(int j = pos_end+1 ; j < _buff_position ; j++, indexBuff++){
//                        _buff[indexBuff] = _buff[j];
//                        //printf("{%02X} ",*(_buff+indexBuff)) ;
//                    }
//                    //printf("\n ") ;
//                    _buff_position = indexBuff;
//
//                    // retourne la taille de la trame
//                    return (pos_end-pos_start)+1;
//                }
//            }
//            //printf("\n") ;
//
//        }
//
//    }while(1);

    int nb_read = 0;
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = timeout_ms*1000;// passage de ms -> us
    // ajout du timeout
    setsockopt(_fileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

    //lecture de la trame
    int isStarted = 0;
    do{

        //if(recv(_fileDescriptor, buff+nb_read, 1, 0)>0){
        if(read(_fileDescriptor, buff+nb_read, 1)>0){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
            if( *(buff+(nb_read-1)) == charEnd){
                *(buff+(nb_read)) = '\0';
                break ;
            }
        }else{
            if ((errno != EAGAIN) && (errno != EWOULDBLOCK)){
                perror("Error readDatas ");
            }
            break;
        }

    }while(1);

    return nb_read;
}

int HILComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff){

    int nb_read = 0;

    //lecture de la trame
    int isStarted = 0;
    do{
        if(recv(_fileDescriptor, buff+nb_read, 1, 0)>0){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
            if( *(buff+(nb_read-1)) == charEnd){
                *(buff+(nb_read)) = '\0';
                break ;
            }
        }else{
            if ((errno != EAGAIN) && (errno != EWOULDBLOCK)){
                perror("Error readDatas ");
            }
            break;
        }

    }while(1);

    return nb_read;
}

int HILComm::writeDatas(const std::string &data){
   //envoie des données
    int ret = 0;

    if( (ret = send(_fileDescriptor, data.c_str() , data.size(), 0)) < 0){
        perror("Send failed : ");
    }
    return ret;
}

int HILComm::writeDatas( void *datas, int sizeDatas ){
    int ret = 0;

    if( (ret = send(_fileDescriptor, datas , sizeDatas, 0)) < 0){
        perror("Send failed : ");
    }
    return ret;
}

void HILComm::disconnect(){
    close(_fileDescriptor);
     _isConnected = false;
}
