/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReadParameters.cpp
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 * 
 * Created on 26 octobre 2016, 10:48
 */

#include "ReadParameters.h"
#include <stdio.h>
#include <algorithm>
#include <unistd.h>



ReadParameters::ReadParameters(string configFile) 
    : _confPathFile(configFile)
{
    
}

ReadParameters::~ReadParameters() {
    
}

Parameters ReadParameters::getParameters(){
    
    Parameters ret;
    if(access(_confPathFile.c_str(), F_OK)){
        cout << "Error: Config file not found" << endl;
        return ret;
    }

    Parameters::iterator it = ret.begin();
    //ouverture du fichier
    _file.open(_confPathFile.c_str());
    
    //parcours du fichier
    while( !_file.eof() ){
        
        // lecture d'une ligne
        string line = "";
        getline(_file, line);
        
        //on récupére le premier caratère différent de ' '
        int posLine = 0;
        while(line[posLine] == ' ')posLine++;
  
        // on verifie que ce n'est pas un commentaire
        // les commentaires commencent par % ou #
        if(line[posLine] != '%' && line[posLine] != '#' && line.size() != 0){
            // suppression des espaces
            line.erase(remove(line.begin(), line.end(), ' '), line.end());

            //printf("%s\n",line.c_str());
            
            // récupération de la clée et de la valeur
            string key = "",value = "";
            int posDelimiter = line.find('=', 0);
            key = line.substr(0, posDelimiter);
            value = line.substr(posDelimiter+1 , line.size()-posDelimiter);
            
            //passage de tous les caractères de la clé en majuscule
            transform(key.begin(), key.end(),key.begin(), ::toupper);
            
            //ajout des paramètres
            ret.insert(it,pair<string,string>(key,value));
        }
    }
    _file.close();
    
    return ret;
            
}
