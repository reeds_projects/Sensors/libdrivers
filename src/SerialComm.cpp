/* 
 * File:   SerialComm.cpp
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 * 
 * Created on 9 septembre 2016, 16:18
 */

#include "SerialComm.h"

#include <stdio.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/msg.h> 
#include <sys/ipc.h>
#include <errno.h>
#include <vector>
#include <sstream>


SerialComm::SerialComm(){
#ifndef SLOW_SYSTEM
    printf("Diver configuration FAST SYSTEM\n");
#else
    printf("Diver configuration SLOW SYSTEM\n");
#endif

}

SerialComm::~SerialComm() {
}

int SerialComm::connection(string path) {
    
    int fd ;
    int result ;
    struct termios paramport ;
    
    // permet de recuperer le port et la vitesse
    vector<string> elems;
    stringstream ss(path.c_str());
    string item;
    while (getline(ss, item, ':')) {
        elems.push_back(item);
    }
    // on verifie que l'on a bien 2 éléments(adresse + port)
    if(elems.size() != 2){
        perror("Error in the format of Path");
        return -1;
    }
    
    //résupération de l'adresse et du port
    const char* nom_device = elems.at(0).c_str();
    int vitesse_param = stoi(elems.at(1).c_str());

    // controle taille max du nom
    if(strlen(nom_device) >= SIZE_NAME_PORT )
    {
        printf("InitSerial: nom_device trop long !") ;
        return(SERIAL_ERR) ;
    }

    fd=open( nom_device , O_RDWR | O_NOCTTY | O_NDELAY) ;
    

    if(fd<0) { printf("InitSerial: open err=%d (%s)\n",errno,strerror(errno)) ; return(fd) ; }

    // recupere la config du port 
    result=tcgetattr( fd , &paramport ) ;
    if(result<0) { printf("InitSerial: tcgetattr err=%d (%s)\n",errno,strerror(errno)) ; return(result) ; }

    // modifie les options 
    
    speed_t vitesse = B0;
    
    switch(vitesse_param) {
        case 300:
            vitesse = B300;
            break;
        case 1200:
            vitesse = B1200;
            break;
        case 2400:
            vitesse = B2400;
            break;
        case 4800:
            vitesse = B4800;
            break;
        case 9600:
            vitesse = B9600;
            break;
        case 14400:
            printf("InitSerial: Erreur vitesse 14400 is not supported \n") ;
            break;
        case 19200:
            vitesse = B19200;
            break;
        case 28800:
            printf("InitSerial: Erreur vitesse 28800 is not supported \n") ;
            break;
        case 38400:
            vitesse = B38400;
            break;
        case 57600:
            vitesse = B57600;
            break;
        case 115200:
            vitesse = B115200;
            break;
        default:
            vitesse = B9600;
            break;
    }
    
    if((result=cfsetispeed( &paramport , vitesse )) < 0 )
        printf("InitSerial: Erreur reglage vitesse in : err=%d (%s)\n",errno,strerror(errno)) ;
    if((result=cfsetospeed( &paramport , vitesse )) < 0 )
        printf("InitSerial: Erreur reglage vitesse out : err=%d (%s)\n",errno,strerror(errno)) ;

    // toujours 8 bits 1 stop sans parite 
    paramport.c_cflag &= ~CSIZE ;	
    paramport.c_cflag &= ~PARENB ;	// pas de parite
    paramport.c_cflag &= ~CSTOPB ;	// 1 seul stop
    paramport.c_cflag |= CS8 ;	// 8 bits

    // controle de flux 
    paramport.c_cflag &= ~CRTSCTS ;		// pas de controle matériel

    // mode raw , pas d'echo , pas de signaux
    paramport.c_lflag &= ~(ICANON | ECHO | ISIG) ;

    // mode d'entrée : pas de gestion xon xoff, pas de conversion CR/LF
    paramport.c_iflag &= ~(IXOFF | IXON | IGNCR | ICRNL ) ;

    // mode de sortie : aucune transformation ou delais
    paramport.c_oflag=0 ;
    //paramport.c_oflag &= ~(ONLCR | OCRNL ) ;

    // politique de reception des caracteres et gestion timeout 
    // pas d'attent , retour immediat avec le nbre de car recu ou ceux disponibles
    paramport.c_cc[VMIN]  =0 ;
    paramport.c_cc[VTIME] =0; 

    // devalide certains caracteres pour pouvoir les utiliser dans les trames
    paramport.c_cc[VKILL] = _POSIX_VDISABLE ; 

    // enfin ecrit la config du port 
    result=tcsetattr( fd , TCSAFLUSH , &paramport ) ;
    if(result<0) { printf("InitSerial:tcsetattr err=%d (%s)\n",errno,strerror(errno)) ; return(result) ; }

    _fileDescriptor=fd ;	// memorise descripteur du port
    printf("InitSerial: Descripteur du port %s -> %d\n",nom_device,_fileDescriptor) ;

     _isConnected = true;

    return(fd) ;

}

void SerialComm::disconnect(){
    // ferme le port
    tcflush(_fileDescriptor,TCIOFLUSH);
    close(_fileDescriptor) ;
    _fileDescriptor=-1 ;
     _isConnected = false;
}

void SerialComm::flush_buffer(){
    tcflush(_fileDescriptor,TCIFLUSH);
}

int SerialComm::writeDatas( const std::string &data ){
    int nb_send ;

    if(_fileDescriptor<0) return(-1) ;    // controle existence port


    flush_buffer();

    // envoi trame
//    printf("write %s \n",data.c_str()) ;
    nb_send= write(_fileDescriptor,data.c_str(),data.size()) ;
//    printf("writed %d \n",nb_send) ;

    return(nb_send) ;
}

int SerialComm::writeDatas( void *datas, int sizeDatas ){
    int nb_send ;
    
    if(_fileDescriptor<0) return(-1) ;    // controle existence port
    
    
    flush_buffer();
    
    nb_send= write(_fileDescriptor,datas,sizeDatas) ;
    //    printf("writed %d \n",nb_send) ;
    
    return(nb_send) ;
}
int SerialComm::readDatas(unsigned char charStart,char *buff,int length){
    int nb_read=0 ;
    bool isStarted = false;
    
    if(_fileDescriptor<0) return(SERIAL_ERR) ;    // controle existence port
    
    // reception de la trame
    do{
        if(read(_fileDescriptor , buff+nb_read , 1) >0 ){
            if(*(buff+nb_read) == charStart) isStarted = true;
            if(isStarted == true){
                nb_read++ ;
            }
            //printf("{%02X}\n",*(buff+nb_read)) ;
        }

        if( nb_read==length)
        {
            break ;
        }

#ifndef SLOW_SYSTEM
    usleep(20) ;
#else
    usleep(200) ; 
#endif

    } while( 1 ) ;
    
    //debug
//    printf("read :");
//    for (int i = 0 ; i < length ; i++)
//        printf(" %x ",buff[i]) ;
//    printf("\n") ;
    ///////////
    return (nb_read) ;
}

int SerialComm::readDatas(char *buff,int length){
    int nb_read=0 ;

    if(_fileDescriptor<0) return(SERIAL_ERR) ;    // controle existence port
    
    // reception de la trame
    do{
        if(read(_fileDescriptor , buff+nb_read , 1) >0 ){
            //printf("{%02X}\n",*(buff+nb_read)) ;
            nb_read++ ;
        }

        if( nb_read==length)
        {
            break ;
        }

#ifndef SLOW_SYSTEM
        usleep(20) ; 
#else
        usleep(200) ; 
#endif

    } while( 1 ) ;
    
    //debug
//    printf("read :");
//    for (int i = 0 ; i < length ; i++)
//        printf(" %x ",buff[i]) ;
//    printf("\n") ;
    ///////////
    return (nb_read) ;
}

int SerialComm::readDatas(char *buff,int length, int timeout_ms){
    int nb_read=0 ;
    bool sortie_timeout=false ;

    if(_fileDescriptor<0) return(SERIAL_ERR) ;    // controle existence port

    //printf("debut ReceiveSerial ") ;

    // arme timeout
    _timeout = std::chrono::system_clock::now();

    // reception de la trame
    do{
        if(read(_fileDescriptor , buff+nb_read , 1) >0 ){
            //printf("{%02X}\n",*(buff+nb_read)) ;
            nb_read++ ;
        }

        if( nb_read==length)
        {
            break ;
        }

#ifndef SLOW_SYSTEM
        usleep(20) ;
#else
        usleep(200) ;
#endif

       
        if( std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - _timeout).count() >= timeout_ms ){
            sortie_timeout=true ;
            break ;
        }

    } while( 1 ) ;
    

    // sortie en timeout ?
    if( sortie_timeout ){
        //printf("TIMEOUT lu=%d taille=%d (handle:%d)\n",nb_read,taille,port_handle) ;
        // retour erreur timeout seulement si aucun car recu
        return(nb_read==0?SERIAL_ERR_TIMOUT:nb_read);
    }

    return(nb_read) ;
}

int SerialComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout_ms){
    int nb_read=0 ;
    bool sortie_timeout=false ;
    int isStarted = 0;
    
    if(_fileDescriptor<0) return(SERIAL_ERR) ;    // controle existence port
    
    //printf("debut ReceiveSerial ") ;
    
    // arme timeout
    _timeout = std::chrono::system_clock::now();
    
    // reception de la trame
    do{
        if(read(_fileDescriptor , buff+nb_read , 1) >0 ){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
        }
        
        if( *(buff+(nb_read-1)) == charEnd){
            *(buff+(nb_read)) = '\0';
            break ;
        }
#ifndef SLOW_SYSTEM
        //usleep(1) ; 
#else
        usleep(20) ; 
#endif
        
        if( std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - _timeout).count() >= timeout_ms ){
            sortie_timeout=true ;
            break ;
        }
        
    } while( 1 ) ;
    
    
    // sortie en timeout ?
    if( sortie_timeout ){
        //printf("TIMEOUT lu=%d taille=%d (handle:%d)\n",nb_read,taille,port_handle) ;
        // retour erreur timeout seulement si aucun car recu
        return(nb_read==0?SERIAL_ERR_TIMOUT:nb_read);
    }
    //printf("trame recue !") ;
    
    return(nb_read) ;
}


int SerialComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff){
    int nb_read=0 ;
    int isStarted = 0;
    
    if(_fileDescriptor<0) return(SERIAL_ERR) ;    // controle existence port
    
    //printf("debut ReceiveSerial ") ;
    
    
    // reception de la trame
    do{
        if(read(_fileDescriptor , buff+nb_read , 1) >0 ){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
        }
        
        if( *(buff+(nb_read-1)) == charEnd){
            *(buff+(nb_read)) = '\0';
            break ;
        }
        
#ifndef SLOW_SYSTEM
        usleep(20) ; 
#else
        usleep(200) ; 
#endif

    } while( 1 ) ;
    
    return(nb_read) ;
}
