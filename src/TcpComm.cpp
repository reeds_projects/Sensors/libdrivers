/* 
 * File:   Tcp2serial.cpp
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 * 
 * Created on 8 septembre 2016, 12:19
 */

#include "TcpComm.h"
#include <vector>
#include <sstream>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#define TCP_ERR		-1
#define TCP_ERR_TIMOUT	-2
#define TCP_ERR_CONNECTION_LOST	-3
#define TCP_SOCK_ERR	-4

//#define __DEBUG_FLAG_

TcpComm::TcpComm() 
{
    //initialisation de la connection TCP
    memset((void *)&_target, 0, sizeof(_target));
    
    _buff_position = 0;
    _lastConnexionInfo ="";
        	
}

TcpComm::~TcpComm() {
    
}

int TcpComm::connection(string server) {

    _lastConnexionInfo = server;

    //printf("Connection tentative: %s\n",server.c_str());

    cout << "Connection tentative:"<<server <<endl;

    
    if(_fileDescriptor == -1){
        //creation de la socket
        _fileDescriptor = socket(AF_INET , SOCK_STREAM , 0);
        if (_fileDescriptor == -1){
            perror("Could not create socket");
        }
    }
    
    // permet de recuperer l'adresse IP et le port du serveur
    vector<string> elems;
    stringstream ss(server.c_str());
    string item;
    while (getline(ss, item, ':')) {
        elems.push_back(item);
    }
    // on verifie que l'on a bien 2 éléments(adresse + port)
    if(elems.size() != 2){
        perror("Error in the format of IP address");
        return -1;
    }
    
    //résupération de l'adresse et du port
    string server_ip = elems.at(0);
    int port = stoi(elems.at(1).c_str());
    
    //setup address structure
    if(inet_addr(server_ip.c_str()) == -1){
        struct hostent *host;
        struct in_addr **addr_list;
         
        //résolution du nom , si ce n'est pas une adresse IP
        if ( (host = gethostbyname( server_ip.c_str() ) ) == NULL){
            herror("gethostbyname\n");    
            return false;
        }
         
        //recupération de la liste d'adresse
        addr_list = (struct in_addr **) host->h_addr_list;
        for(int i = 0; addr_list[i] != NULL; i++){
            _target.sin_addr = *addr_list[i];
            break;
        }
    }else{
        _target.sin_addr.s_addr = inet_addr( server_ip.c_str() );
    }
     
    _target.sin_family = AF_INET;
    _target.sin_port = htons( port );
     
    //Connexion
    int ret = 0;
    if ((ret = connect(_fileDescriptor, (struct sockaddr *)&_target , sizeof(_target))) < 0){
        close(_fileDescriptor);
        perror("connect failed. Error\n");
    }else{
        _isConnected = true;
    }

    return ret;

}

int TcpComm::readDatas(char *buff,int length){

    int ret = 0;

    //on check que l'on est connecté
    if(!fileDescriptorIsValid()) return TCP_SOCK_ERR;

    //réception depuis le serveur
    int nb_read = 0;
    while(1){
        if ((nb_read += recv(_fileDescriptor, buff+nb_read, length-nb_read,0)) < 0){
            if ((errno==ENOTCONN) || (errno==EPIPE)){
                perror("Error readDatas :");
                //strcpy (buff,CONNECTION_RESET_BY_PEER);
                //printf("TcpComm::readDatas: re-connection = %d\n",connectionLost()) ;
                return TCP_ERR_CONNECTION_LOST;
            }else{
                perror("Error readDatas :");
                return TCP_ERR;
            }
        }

        if(nb_read >= length) break;
    }
    return nb_read;
}

//int TcpComm::readDatas(char *buff,int length, int timeout_ms){

//    int ret = 0;

//    //on check que l'on est connecté
//    if(!fileDescriptorIsValid())return TCP_SOCK_ERR;

//#ifdef __DEBUG_FLAG_
//    printf("TcpComm::readDatas(%d,%d)\n",length,timeout_ms);

//#endif
//    int nb_read = 0;
//    struct timeval timeout;
//    timeout.tv_sec = timeout_ms/1000;
//    timeout.tv_usec = (timeout_ms - ((timeout_ms/1000)*1000) )*1000;// passage de ms -> us
//    // ajout du timeout
//    if( setsockopt(_fileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0){
//        cout << "BaseDriver :: TcpComm::readDatas -- ERROR : Error configuration Timeout "<<endl;
//    }

//    _timeout = std::chrono::system_clock::now();

//    bool sortie_timeout = false;
//        int to_read=length ;
//        char *ptr ;


//    ptr=buff ;
//    do
//    {
//        if((nb_read=recv(_fileDescriptor , ptr , to_read, 0)) >0 )
//        {

//            to_read-=nb_read ;
//            ptr+=nb_read ;

//            if( to_read==0) // tout recu ?
//                break ;

//        }
//        else if( (errno==ENOTCONN) || (errno==EPIPE) )    // erreur de connection !
//        {
//            int conn = connectionLost();
//            //printf("TcpComm::writeDatas: re-connection = %d\n",conn) ;
//            cout << "BaseDriver :: TcpComm::readDatas: re-connection = "<<conn <<endl;
//            return(TCP_ERR_CONNECTION_LOST) ;
//        }
//        else    // autres erreurs ...
//        {
//            sortie_timeout=true ;   // sortie en echec
//            break ;
//        }

//        usleep(50) ;   //  pour eviter attente active ( a voir ... )


//    } while( 1 ) ;




////#ifdef __DEBUG_FLAG_
////    printf("TcpComm::readDatas(%d,%d) -- received : %d\n",length,timeout_ms,nb_read);
////#endif

//    if( sortie_timeout ){
//        return(to_read==0?TCP_ERR_TIMOUT:length-to_read);
//    }

//    return(length-to_read) ;
//}

int TcpComm::readDatas(char *buff,int length, int timeout_ms){

    int ret = 0;

    //on check que l'on est connecté
    if(!fileDescriptorIsValid())return TCP_SOCK_ERR;

#ifdef __DEBUG_FLAG_
    printf("TcpComm::readDatas(%d,%d)\n",length,timeout_ms);

#endif
    int nb_read = 0;
    struct timeval timeout;
    timeout.tv_sec = timeout_ms/1000;
    timeout.tv_usec = (timeout_ms - ((timeout_ms/1000)*1000) )*1000;// passage de ms -> us
    // ajout du timeout
    if( setsockopt(_fileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0){
        cout << "BaseDriver :: TcpComm::readDatas -- ERROR : Error configuration Timeout "<<endl;
    }

    _timeout = std::chrono::system_clock::now();

    bool sortie_timeout = false;

    //lecture de la trame
    while(1){
#ifdef __DEBUG_FLAG_
    printf("TcpComm::readDatas(%d,%d) -- avant recv\n",length,timeout_ms);
#endif
        if ((nb_read += recv(_fileDescriptor, buff+nb_read, length-nb_read,0)) < 0){
            cout << "BaseDriver :: TcpComm::readDatas -- ERROR : "<<strerror(errno)<<" = "<<errno <<endl;
            // si erreur
            if( (errno==ENOTCONN) || (errno==EPIPE))    // erreur de connection !
            {
                //strcpy (buff,CONNECTION_RESET_BY_PEER);
                //printf("TcpComm::readDatas: re-connection = %d\n",connectionLost()) ;
                return(TCP_ERR_CONNECTION_LOST) ;
            }else if ((errno==EAGAIN)){
                return(TCP_ERR_TIMOUT) ;
            }else{
                 //strcpy (buff,strerror(errno));
                 return(TCP_ERR) ;
            }

        }
#ifdef __DEBUG_FLAG_
    printf("TcpComm::readDatas(%d,%d) -- apres recv\n",length,timeout_ms);
#endif

        if( std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - _timeout).count() >= timeout_ms ){
            sortie_timeout=true ;
            //perror("Error Timeout");
            break ;
        }

        if(nb_read >= length) break;
    }



//#ifdef __DEBUG_FLAG_
//    printf("TcpComm::readDatas(%d,%d) -- received : %d\n",length,timeout_ms,nb_read);
//#endif

    if( sortie_timeout ){
        //printf("TIMEOUT lu=%d taille=%d (handle:%d)\n",nb_read,taille,port_handle) ;
        // retour erreur timeout seulement si aucun car recu
        //strcpy (buff,COMMUNICATION_TIMEOUT);
        return(nb_read==0?TCP_ERR_TIMOUT:nb_read);
    }
    
    return nb_read;
}

int TcpComm::readDatas(unsigned char charStart,char *buff,int length){

    int ret = 0;

    //on check que l'on est connecté
    if(!fileDescriptorIsValid())return TCP_SOCK_ERR;

    int nb_read=0 ;
    bool isStarted = false;

    // reception de la trame
    do{
        if(recv(_fileDescriptor , buff+nb_read , 1,0) >0 ){
            if(*(buff+nb_read) == charStart) isStarted = true;
            if(isStarted == true){
                nb_read++ ;
            }
            //printf("{%02X}\n",*(buff+nb_read)) ;
        }else if ((errno==ENOTCONN) || (errno==EPIPE)){
            perror("Error readDatas :");
            //strcpy (buff,CONNECTION_RESET_BY_PEER);
            //printf("TcpComm::readDatas: re-connection = %d\n",connectionLost()) ;
            return TCP_ERR_CONNECTION_LOST;
        }else{
            perror("Error readDatas :");
            return TCP_ERR;
        }

        if( nb_read==length)
        {
            break ;
        }

    } while( 1 ) ;

    //debug
//    printf("read :");
//    for (int i = 0 ; i < length ; i++)
//        printf(" %x ",buff[i]) ;
//    printf("\n") ;
    ///////////
    return (nb_read) ;
}

int TcpComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout_ms){

    int ret = 0;

    //on check que l'on est connecté
    if(!fileDescriptorIsValid())return TCP_SOCK_ERR;
    
    int nb_read = 0;
    struct timeval timeout;
    timeout.tv_sec = timeout_ms/1000;
    timeout.tv_usec = (timeout_ms - ((timeout_ms/1000)*1000) )*1000;// passage de ms -> us
    // ajout du timeout
    if( setsockopt(_fileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0){
        perror("Error configuration Timeout ");
    }

    _timeout = std::chrono::system_clock::now();

    bool sortie_timeout = false;
    
    //lecture de la trame
    int isStarted = 0;
    do{
        
        //if(recv(_fileDescriptor, buff+nb_read, 1, 0)>0){
        if(recv(_fileDescriptor, buff+nb_read, 1,0)>0){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
            if( *(buff+(nb_read-1)) == charEnd){
                *(buff+(nb_read)) = '\0';
                break ;
            }
        }else{
            if ((errno==ENOTCONN) || (errno==EPIPE)){
                perror("Error readDatas :");
                //strcpy (buff,CONNECTION_RESET_BY_PEER);
                //printf("TcpComm::readDatas: re-connection = %d\n",connectionLost()) ;
                return TCP_ERR_CONNECTION_LOST;
            }else{
                perror("Error readDatas :");
                return TCP_ERR;
            }
        }


        if( std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - _timeout).count() >= timeout_ms ){
            sortie_timeout=true ;
            break ;
        }
        
    }while(1);

    if( sortie_timeout ){
        //printf("TIMEOUT lu=%d taille=%d (handle:%d)\n",nb_read,taille,port_handle) ;
        // retour erreur timeout seulement si aucun car recu
        return(nb_read==0?TCP_ERR_TIMOUT:nb_read);
    }
    
    return nb_read;
}

int TcpComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff){
    
    int nb_read = 0;
    
    //lecture de la trame
    int isStarted = 0;
    do{
        if(recv(_fileDescriptor, buff+nb_read, 1,0)>0){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
            if( *(buff+(nb_read-1)) == charEnd){
                *(buff+(nb_read)) = '\0';
                break ;
            }
        }else{
            if ((errno==ENOTCONN) || (errno==EPIPE)){
                perror("Error readDatas :");
                //strcpy (buff,CONNECTION_RESET_BY_PEER);
                //printf("TcpComm::readDatas: re-connection = %d\n",connectionLost()) ;
                return TCP_ERR_CONNECTION_LOST;
            }else{
                perror("Error readDatas :");
                return TCP_ERR;
            }
        }
        
    }while(1);
    
    return nb_read;
}

int TcpComm::writeDatas(const std::string &data){

    int ret = -1;
    int retry=0 ;
    //on check que l'on est connecté
    if(!fileDescriptorIsValid())return TCP_SOCK_ERR;

#ifdef __DEBUG_FLAG_
    for(int i = 0 ; i < sizeDatas ; i++)printf("%02x ",((char*)datas)[i]);
    printf("\n");
#endif

    while( (ret<0) && (retry++ <= 2) )
    {

        if( (ret = send(_fileDescriptor, data.c_str() , data.size(), MSG_NOSIGNAL + MSG_DONTWAIT)) == data.size()){
            return ret;
        }else{
            printf("Send failed (%d/3): %s",retry,strerror(errno));

            if( (errno==ENOTCONN) || (errno==EPIPE) )
               {
                   //printf("TcpComm::writeDatas: re-connection = %d\n",connectionLost()) ;
                   disconnect();
                   return TCP_ERR_CONNECTION_LOST;
               }

        }

    }
    return TCP_ERR;
}

int TcpComm::writeDatas( void *datas, int sizeDatas ){
    int ret = -1;
    int retry=0 ;
    //on check que l'on est connecté
    if(!fileDescriptorIsValid())return TCP_SOCK_ERR;

#ifdef __DEBUG_FLAG_
    for(int i = 0 ; i < sizeDatas ; i++)printf("%02x ",((char*)datas)[i]);
    printf("\n");
#endif
    while( (ret<0) && (retry++ <= 2) )
    {

        if( (ret = send(_fileDescriptor, datas , sizeDatas, MSG_NOSIGNAL + MSG_DONTWAIT)) == sizeDatas){
            return ret;
        }else{
            printf("Send failed (%d/3): %s",retry,strerror(errno));

            if( (errno==ENOTCONN) || (errno==EPIPE) )
               {
                   //int conn = connectionLost();
                   //printf("TcpComm::writeDatas: re-connection = %d\n",conn) ;
                   //cout << "TcpComm::writeDatas: re-connection = "<<conn <<endl;
                    disconnect();
                   return TCP_ERR_CONNECTION_LOST;
               }

        }
    }
    return TCP_ERR;
}

void TcpComm::disconnect(){
    //printf("fileDescriptorIsValid %d\n",fileDescriptorIsValid());
    _isConnected = false;
    if(fileDescriptorIsValid()){
        tcflush(_fileDescriptor,TCIOFLUSH);
        close(_fileDescriptor);
        _fileDescriptor = -1;
    }

    // desinstalle handler du signal
    signal(SIGPIPE,SIG_DFL) ;


}

int TcpComm::connectionLost(){
    _isConnected = false;
    tcflush(_fileDescriptor,TCIOFLUSH);
    close(_fileDescriptor);
    _fileDescriptor = -1;
    usleep(50000);

    int ret = connection(_lastConnexionInfo);
    usleep(100000);

    return ret;


}
