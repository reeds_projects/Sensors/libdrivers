/* 
 * File:   UdpComm.cpp
 * Author: ROPARS Benoît < ropars.benoit at gmail.fr>
 * 
 * Created on 9 septembre 2016, 14:37
 */

#include "UdpComm.h"
#include <vector>
#include <sstream>

UdpComm::UdpComm() 
{
    //initialisation de la connection UDP
    memset((void *)&_target, 0, sizeof(_target));
}

UdpComm::~UdpComm() {}


int UdpComm::connection(string server) {
    
    if(_fileDescriptor == -1){
        //creation de la socket
        _fileDescriptor = socket(AF_INET , SOCK_DGRAM ,  IPPROTO_UDP);
        if (_fileDescriptor == -1){
            perror("Could not create socket");
        }
    }
    
    // permet de recuperer l'adresse IP et le port du serveur
    vector<string> elems;
    stringstream ss(server.c_str());
    string item;
    while (getline(ss, item, ':')) {
        elems.push_back(item);
    }
    bool serverMode = false;
    // on verifie que l'on a bien 2 éléments(adresse + port)
    if(elems.size() < 2 && elems.size()>3){
        perror("Error in the format of IP address");
        return -1;
    }else{
        if(elems.size() == 3){
            if(elems.at(2) == "-S" || elems.at(2) == "-s"){
                serverMode = true;
            }
        }
    }
    
    //résupération de l'adresse et du port
    string server_ip = elems.at(0);
    int port = stoi(elems.at(1).c_str());

    if(serverMode)printf("UDP SERVER MODE : ip : %s , port : %d \n",server_ip.c_str(),port);
    
    //printf("ip : %s , port : %d\n",server_ip.c_str(),port);
    
    //setup address structure
    if(inet_addr(server_ip.c_str()) == -1){
        printf("*******\n");
        struct hostent *host;
        struct in_addr **addr_list;
         
        //résolution du nom , si ce n'est pas une adresse IP
        if ( (host = gethostbyname( server_ip.c_str() ) ) == NULL){
            herror("gethostbyname\n");    
            return false;
        }
         
        //recupération de la liste d'adresse
        addr_list = (struct in_addr **) host->h_addr_list;
        for(int i = 0; addr_list[i] != NULL; i++){
            _target.sin_addr = *addr_list[i];
            break;
        }
    }else{
        _target.sin_addr.s_addr = inet_addr( server_ip.c_str() );
    }
     
    _target.sin_family = AF_INET;
    _target.sin_port = htons( port );
    
    //Connexion
    int ret = 0;

//    int flag = fcntl( _fileDescriptor, F_GETFL, 0 );
//    fcntl( _fileDescriptor, F_SETFL, flag | O_NONBLOCK );


    if(serverMode){
        printf("Bind UDP socket\n");
        if (::bind(_fileDescriptor, (struct sockaddr *)&_target , sizeof(_target)) < 0){
            close(_fileDescriptor);
            perror("connect failed. Error\n");
            return -2;
        }
        printf("Bind UDP socket : OK\n");
    }else{

//    if ((ret = bind(_fileDescriptor , (struct sockaddr *)&_target , sizeof(_target))) < 0){
//        close(_fileDescriptor);
//        perror("connect failed. Error\n");
//    }
    
//        if ((ret = bind(_fileDescriptor , (struct sockaddr *)&_target , sizeof(_target))) < 0){
//        close(_fileDescriptor);
//        perror("connect failed. Error\n");
//    }
            


        // contact server
        if((ret = sendto(_fileDescriptor, "OK", 2, 0, (struct sockaddr*)&_target, sizeof(_target))) < 0){
            printf("InitUdp: Erreur contact serveur") ;
        }else printf("Contact serveur en %s:%u",server_ip.c_str(),port) ;
    }

    _isConnected = true;

    return ret;

}

int UdpComm::readDatas(char *buff,int length){
    
    //réception depuis le serveur
    int serverlen = sizeof(_target);
    int nb_read = 0; 
    
    if( (nb_read = recvfrom(_fileDescriptor , buff , length, 0, (struct sockaddr *)&_target , (socklen_t *)&serverlen)) < 0){
        puts("recv failed");
    }
    //printf("%d,%s",nb_read,buff );
    return nb_read;
}

int UdpComm::readDatas(char *buff,int length, int timeout_ms){
    
    int nb_read = 0;
    
    struct timeval timeout;
    timeout.tv_sec = timeout_ms/1000;
    timeout.tv_usec = (timeout_ms - ((timeout_ms/1000)*1000) )*1000;// passage de ms -> us
    
    // ajout du timeout
    if( setsockopt(_fileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0){
        perror("Error configuration Timeout ");
    }

    //lecture de la trame
    int serverlen = sizeof(_target);
    if ((nb_read = recvfrom(_fileDescriptor , buff , length, 0, (struct sockaddr *)&_target , (socklen_t *)&serverlen)) == -1){
        // si erreur
        if ((errno != EAGAIN) && (errno != EWOULDBLOCK)){
            perror("Error readDatas ");
        }
    }
    
    return nb_read;
}

int UdpComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff){
    
    int nb_read = 0;
    
    //lecture de la trame
    int serverlen = sizeof(_target);
    int isStarted = 0;
    do{
        if(recvfrom(_fileDescriptor , buff+nb_read , 1, 0, (struct sockaddr *)&_target , (socklen_t *)&serverlen)>0){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
            if( *(buff+(nb_read-1)) == charEnd){
                *(buff+(nb_read)) = '\0';
                break ;
            }
        }else{
            if ((errno != EAGAIN) && (errno != EWOULDBLOCK)){
                perror("Error readDatas ");
            }
            break;
        }
        
    }while(1);
    
    return nb_read;
}

int UdpComm::readDatas(unsigned char charStart,unsigned char charEnd,char *buff, int timeout_ms){
    
    int nb_read = 0;
    
    struct timeval timeout;

    //printf("timeout sec = %d , usec = %d\n",0, timeout*1000);
    timeout.tv_sec = timeout_ms/1000;
    timeout.tv_usec = (timeout_ms - ((timeout_ms/1000)*1000) )*1000;// passage de ms -> us

    
    // ajout du timeout
    if( setsockopt(_fileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0){
        perror("Error configuration Timeout");
    }
    
    //lecture de la trame
    int serverlen = sizeof(_target);
    int isStarted = 0;
    do{
        if(recvfrom(_fileDescriptor , buff+nb_read , 1, 0, (struct sockaddr *)&_target , (socklen_t *)&serverlen)>0){
            if(*(buff+nb_read) == charStart) isStarted = 1;
            if(isStarted == 1){
                //printf("{%02X} = %c \n",*(buff+nb_read),*(buff+nb_read)) ;
                nb_read++ ;
            }
            if( *(buff+(nb_read-1)) == charEnd){
                *(buff+(nb_read)) = '\0';
                break ;
            }
        }else{
            if ((errno != EAGAIN) && (errno != EWOULDBLOCK)){
                perror("Error readDatas ");
            }
            break;
        }
        
    }while(1);
    
    return nb_read;
}

int UdpComm::writeDatas(const std::string &data){
   
    int ret = 0;
    //envoie des données
    int serverlen = sizeof(_target);
    if( (ret = sendto(_fileDescriptor , data.c_str() , data.size(),0 ,(struct sockaddr *) &_target ,serverlen)) < 0){
        perror("Send failed : ");
    }
    return ret;
}

int UdpComm::writeDatas( void *datas, int sizeDatas ){
    int ret = 0;
    int serverlen = sizeof(_target);
    if( (ret = sendto(_fileDescriptor , datas , sizeDatas,0 ,(struct sockaddr *) &_target ,serverlen)) < 0){
        perror("Send failed : ");
    }
    return ret;
}

void UdpComm::disconnect(){
     _isConnected = false;
    close(_fileDescriptor);
}
